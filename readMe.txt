
GUIDANCE: SEWA's DOCKER IMAGE - BASED ON UBUNTU 16.04

This readMe file contains the instructions from installing docker to run SEWA Demos in images.

##############################
Step 1-3: Install & Config Docker
Step 4-5: Build or pull sewa_image & run a container from the image; SSH to the container
Step 6: Run Demo through SSH
Step 7: Using the web server
##############################

1.Install Docker
For ubuntu 16.04, run the following commands to install Docker:
sudo aptitude update
sudo aptitude -y upgrade
sudo aptitude install linux-image-extra-`uname -r`
sudo sh -c "wget -qO- https://get.docker.io/gpg | apt-key add -"
sudo sh -c "echo deb http://get.docker.io/ubuntu docker main\
	> /etc/apt/sources.list.d/docker.list"
sudo aptitude update
sudo aptitude install lxc-docker

2. Enabling forwarding in UFW
Ubuntu's default firewall (UFW: Uncomplicated Firewall) denies all forwarding traffic by default, which is needed by docker.

First enable forwarding with UFW. Open UFW file:
sudo gedit /etc/default/ufw

Then Scroll down and find the line beginning with DEFAULT_FORWARD_POLICY.

Replace:
DEFAULT_FORWARD_POLICY="DROP"

With:
DEFAULT_FORWARD_POLICY="ACCEPT"

Save it. Then reload the UFW:
sudo ufw reload


3. Configure docker
After the installation of docker, run the following command (replace '<username>' with the current username):
sudo su -
service docker start
docker images
sudo usermod -aG docker <username>

Then add DNS address to avoid potential problems. Open the following file:
sudo gedit /etc/docker/daemon.json

Then copy and paste the following content to it and save:
{"dns": ["8.8.8.8", "8.8.2.2"]}

Then restart the system. Run a hello-world daemon to see if docker works:
sudo service docker start
sudo docker run hello-world

Before starting the next step, please make sure the docker's hello-world daemon works.


For step 4, you have two options, and only need to do one of them: 
4a(Recommanded): Pull a pre-compiled sewa image from docker hub;
4b: Build the sewa image from the package youself
You can select either 4a or 4b. 4a is simpler and quicker, and is therefore recommanded.

4a. Pull the sewa image from docker hub
You can pull the pre-compiled sewa image from docker hub using the command:
docker pull mapleandfire/sewa_image

If everything works well, you can find the mapleandfire/sewa_image in your image list by entering:
docker images


4b. Build the docker image from the package youself
You can also choose to build the sewa_image yourself.
First CD into the root folder of this package (where you can find a 'Dockerfile', e.g.:/home/yujiang/SEWA/docker/SewaImage), then build the image:
sudo docker build -t sewa_image .

This step may take several minutes. If successful, a docker image named 'sewa_image' will be created. 
You can check all you images by entering:
docker images

Then add a tag to it:
docker tag -f sewa_image mapleandfire/sewa_image:latest


5. Start the SSH server in container and test it
Run the container of this image and expose port 222 for ssh server:
sudo docker run --name sewa -p 222:22 -p 8080:8080 -i -t mapleandfire/sewa_image

If you already have a container named sewa, you need to first remove (if no data is needed) or rename it,
docker rm -f sewa

or

docker rename sewa sewa_old

then start the container:
sudo docker run --name sewa -p 222:22 -p 8080:8080 -i -t mapleandfire/sewa_image

Now that the container is running, you can SSH to the container in another computer (or in the same computer) using the following 
command (simple replace 'ipAddress' with your host's ip address, e.g.: 146.169.24.110; password: ibug):
ssh root@ipAddress -p 222

You can enter the filesystem of the container by entering:
cd /source
ls


6. Run Demos when you have ssh into the image.
For this step, please make sure you have successfully completed Step 5, i.e. run the container in server and ssh into it in client.
The following commands in this step are run on the ssh client.

First add miniconda to system, so that we can use 'python' cmd:
export PATH=$PATH:/miniconda/bin

a. Run audio demo:
cd /source/sewa
sh test_sewa_audio_toolchain.sh

b. Run video demo:
cd /source/sewa
sh test_sewa_video_toolchain.sh


7. Using web server

If you want to use web server, please first update your sewa iamge (just do step 4a again)

Then do step 5 to make sure the sewa container is running in host, and open a new terminal in the host, then enter to start the web server:
sudo docker exec -i -t sewa /miniconda/bin/python ./server.py

Now the webserver should be started from the container. So in any client, you can upload two files (video and audio) to host for processing.

Currently we provide two approaches to upload files. You can try more if you want.

a. Upload through web browser. Open a web browser like Firefox, then enter the following in the address line: 
http://ipAddress:8080
where ipAddress is your hosts ip adress, like 146.169.24.120

Then you can upload file by the instructions in the website. 
NOTE: you should select exactly two files, one for video with extension '.avi' or '.mp4', another for audio with extension '.wav'.

After uploading, please wait for the processing to finished. Once done, you should be able to download a csv file with all results in it.


b. Upload using python scripts. The approach is recommanded as it will save you some time.

For this approach, please refer to the example python script 'uploadFile.py'. You need to change the ip address, video and audio's name and path

###############################
To restart a stopped container:
docker start CONTAINER_ID

To stop it, just use
docker stop CONTAINER_ID

Where CONTAINER_ID is the container's ID that you just run, you can find it by:
docker ps -a

If you know the container's name, for example 'sewa' as we created before, you can simply use:
docker start sewa
docker stop sewa
##############################
Stop all docker containers:
docker stop $(docker ps -a -q)

Remove all untagged images:
docker rmi $(docker images | grep "^<none>" | awk "{print $3}")

Remove all stopped containers:
docker rm $(docker ps -a -q) 

###############################
Commit an image:
You can save the changes in one image (changed by the running container) by using commit:
docker commit CONTAINER_ID COMMIT_IMAGE_NAME

COMMIT_IMAGE_NAME is the new image which includes the changes implemented by the container.

###############################
If you have installed docker, and you are running into 'docker service cannot start' problems, you may try:
rm -rf /var/lib/docker/

And then reinstall docker from beginning.

