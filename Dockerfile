############################################################
# Dockerfile to build SEWA image
# Based on Ubuntu
############################################################


# Set the base image to Ubuntu
FROM ubuntu:16.04


# Update the sources list
RUN apt-get update && apt-get install -y apt-transport-https


# Add the application resources URL
RUN echo "deb http://archive.ubuntu.com/ubuntu/ $(lsb_release -sc) main universe" >> /etc/apt/sources.list


# Install basic and necessary applications
RUN apt-get install -y gedit tar git curl wget dialog net-tools build-essential libjpeg-turbo8 libfontconfig1 default-jre libgtk2.0-0 liblist-moreutils-perl 


# Install ssh
RUN apt-get install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:ibug' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# Install miniconda
RUN curl -LO https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
RUN bash Miniconda2-latest-Linux-x86_64.sh -p /miniconda -b
RUN rm Miniconda2-latest-Linux-x86_64.sh
ENV PATH=/miniconda/bin:${PATH}
RUN conda update -y conda

# Install python Dependencies
RUN conda install -y numpy matplotlib scikit-learn scikit-image
RUN conda install -y -c conda-forge imageio
RUN conda install -y -c menpo opencv
RUN conda install -y -c menpo dlib
RUN conda install -y -c omnia cvxopt
RUN conda install -y -c anaconda pandas
RUN pip install skll
RUN pip install --user --upgrade --no-deps pystruct

# Install Python Server Dependencies
RUN pip install cherrypy
RUN pip install ffmpy

# install ffmepg for imageio
RUN apt-get install -y ffmpeg vim nano 

# Copy the application folder inside the container
ADD ./source ./source

# Expose ports for ssh server
EXPOSE 22

# Start the ssh server, password: ibug
CMD sh /source/startCMD.sh

# Set the default directory 
WORKDIR /source/sewa/sewa_server/

# Expose ports for python server
EXPOSE 8080

ENTRYPOINT ["/miniconda/bin/python", "./server.py"]