import os
import sys
import getopt


def main(argv):
    input_file = None
    output_file = None
    opts, args = getopt.getopt(argv, 'i:o:', ['ifile=', 'ofile='])
    for opt, arg in opts:
        if opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg

    process(input_file, output_file)


def process(input_file, output_file):
    if input_file is None:
        print 'Please specify the input LIBSVM file path!'
    elif output_file is None:
        print 'Please specify the output CSV file path!'
    else:
        print 'Input LIBSVM file path: ' + input_file
        print 'Output CSV file path: ' + output_file
        input_file = os.path.realpath(input_file)
        output_file = os.path.realpath(output_file)
        output_valence = output_file + '.valence'
        output_arousal = output_file + '.arousal'
        output_liking = output_file + '.liking'
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        if os.name == 'nt':
            os.system('predict.exe "' + input_file + '" modelValence.svr "' + output_valence + '"')
            os.system('predict.exe "' + input_file + '" modelArousal.svr "' + output_arousal + '"')
            os.system('predict.exe "' + input_file + '" modelLiking.svr "' + output_liking + '"')
        else:
            os.system('./predict "' + input_file + '" modelValence.svr "' + output_valence + '"')
            os.system('./predict "' + input_file + '" modelArousal.svr "' + output_arousal + '"')
            os.system('./predict "' + input_file + '" modelLiking.svr "' + output_liking + '"')
        with open(output_valence) as valence, open(output_arousal) as arousal, open(output_liking) as liking:
            output = open(output_file, 'w')
            output.write('Valence,Arousal,Liking\n')
            output.write('{0},{1},{2}\n'.format(float(valence.read()), float(arousal.read()), float(liking.read())))
            output.close()
        os.remove(output_liking)
        os.remove(output_arousal)
        os.remove(output_valence)
        print 'Valence, arousal, and liking levels estimated.'


if __name__ == "__main__":
    main(sys.argv[1:])
