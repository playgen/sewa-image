#!/usr/bin/env bash
source activate sewa
python sewa_val_predictor.py -i "../test_data/Round2_SessionId_46/BOAW.libsvm" \
    -o "../test_data/Round2_SessionId_46/VAL.csv"
source deactivate sewa
