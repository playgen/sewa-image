# the python script using flash to upload files

import numpy as np
import os
from flask import Flask, render_template, request, redirect, url_for, send_file
from werkzeug.utils import secure_filename
import subprocess
import pandas as pd
from moviepy.editor import *

# Initialize the Flask application
app = Flask(__name__)

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = './uploads/'
app.config['PROCESSING_FOLDER'] = './processing/'
app.config['RESULT_FOLDER'] = './results/'

# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['avi', 'mp4', 'wav'])

# These are the extension that we are accepting to be uploaded
app.config['CONVERTABLE_EXTENSIONS'] = set(['webm'])

# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

# For a given file, return whether it's a type we can convert or not
def convertable_file(filename):
    return filename.rsplit('.', 1)[1] in app.config['CONVERTABLE_EXTENSIONS']

@app.route('/')
def index():
    return render_template('Index.html')


# upload audio and video
@app.route('/upload', methods=['POST'])
def upload():

    if request.method == 'POST':

        # first clean the old upload files and results
        # if not os.listdir(app.config['UPLOAD_FOLDER']):
        cleanCmd = ['bash', 'clean_processing_upload_result_data.sh']
        subprocess.call(cleanCmd)

        print('Uploading files')

        files = request.files.getlist('files')
        ulName = []

        for file in files:

            if file:
                filename = secure_filename(file.filename)
                # uploadName(filename)
                print('Filename is '+filename)

                if allowed_file(filename):
                    # Save and add to upload list
                    ulName.append(filename)
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))


                elif convertable_file(filename):
                    # Convert to .mp4 and .wav combination
                    processingPath = os.path.join(app.config['PROCESSING_FOLDER'], filename);                    
                    filenameWithoutExtension = os.path.basename(filename)
                    file.save(processingPath)

                    video = VideoFileClip(processingPath)
                    
                    audioFilename = filenameWithoutExtension + ".wav"
                    video.audio.write_audiofile(os.path.join(app.config['UPLOAD_FOLDER'], audioFilename))
                    ulName.append(audioFilename)

                    videoFilename = filenameWithoutExtension + ".mp4"
                    video = video.without_audio()
                    video.write_videofile(os.path.join(app.config['UPLOAD_FOLDER'], videoFilename));
                    ulName.append(videoFilename)


        print('Uploading completed')
        nameList = ','.join(ulName)

        return process_data(nameList)
        #return redirect(url_for('process_data',ulName=nameList))


# process audio and video
@app.route('/processData/<ulName>')
def process_data(ulName):

    ulName = ulName.split(',')

    if len(ulName) != 2:
        print('Incorrect number of uploaded files or file name contain commas! Please upload again!')
        return redirect(url_for('index'))

    # save filename of video and audio
    if (ulName[0].endswith('avi') or ulName[0].endswith('mp4')) and ulName[1].endswith('wav'):
        videoName = ulName[0]
        audioName = ulName[1]
    elif (ulName[1].endswith('avi') or ulName[1].endswith('mp4')) and ulName[0].endswith('wav'):
        videoName = ulName[1]
        audioName = ulName[0]
    else:
        print('Incorrect upload! Please upload again!')
        return redirect(url_for('index'))

    # the path of uploaded video and audio
    videoPath = app.config['UPLOAD_FOLDER'] + videoName
    audioPath = app.config['UPLOAD_FOLDER'] + audioName

    vidResPath = app.config['RESULT_FOLDER']+'video'
    audResPath = app.config['RESULT_FOLDER']+'audio'

    # call video script to process upload video
    bashCmdVideo = ['bash', 'sewa_video_toolchain_web.sh', videoPath, vidResPath]
    subprocess.call(bashCmdVideo)

    # call audio script to process upload audio
    bashCmdVideo = ['bash', 'sewa_audio_toolchain_web.sh', audioPath, audResPath]
    subprocess.call(bashCmdVideo)

    print("All processing is completed, saving result")

    vidCsvPath = app.config['RESULT_FOLDER']+'video/'+'AUs.csv'
    vidDf = pd.read_csv(vidCsvPath)

    excitement = round((np.mean(vidDf['AU01']) + np.mean(vidDf['AU02']))/2.0,6)
    negativity = round(np.mean(vidDf['AU04']),6)
    positivity = round(np.mean(vidDf['AU12']),6)
    confusion = round(np.mean(vidDf['AU17']),6)

    audCsvPath = app.config['RESULT_FOLDER']+'audio/'+'VAL.csv'
    audDf = pd.read_csv(audCsvPath)
    valence = audDf['Valence'][0]
    arousal = audDf['Arousal'][0]
    liking = audDf['Liking'][0]

    res_data = {'Excitement': [excitement],'Negativity': [negativity],
                'Positivity': [positivity],'Confusion': [confusion],
                'Valence': [valence],'Arousal': [arousal],
                'Liking': [liking]}
    df = pd.DataFrame(res_data, columns=['Excitement', 'Negativity', 'Positivity', 'Confusion',
                                         'Valence','Arousal','Liking'])

    saveName = app.config['RESULT_FOLDER'] + 'result.csv'
    df.to_csv(saveName,index=False)

    print("Result saved")

    return show_result();
    #return redirect(url_for('show_result'))



# get high-level results from both audio and video results
@app.route("/results/")
def show_result():

    saveName = app.config['RESULT_FOLDER'] + 'result.csv'
    return send_file(saveName, mimetype='text/csv')


if __name__ == "__main__":
    app.run()
