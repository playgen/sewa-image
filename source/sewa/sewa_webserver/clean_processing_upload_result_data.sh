#!/usr/bin/env bash
echo Cleaning Old Upload Files and Results
echo Removing Uploads
rm -rf ./uploads/
mkdir ./uploads/
echo Removing Results
rm -rf ./results/
mkdir ./results/video/
mkdir ./results/audio/
echo Removing Processing
rm -rf ./processing/
mkdir ./processing/
echo Cleaning Completed
