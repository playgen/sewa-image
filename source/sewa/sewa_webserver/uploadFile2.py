# sample script for uploading files and receiving results

import requests

if __name__ == '__main__':

    # please change the ip address to your host's ip adress
    ipAdress = 'http://librarian.playgen.internal'

    # please change the video name and video path!
    videoName = 'e68b715c-9954-4ae2-899f-d554f3de5eb2.avi'
    videoPath = 'C:/temp/archives/c26f727f-3392-4f5b-b331-b4f5f527df97/e68b715c-9954-4ae2-899f-d554f3de5eb2.avi'

    # please change the audio name and audio path!
    audioName = 'e68b715c-9954-4ae2-899f-d554f3de5eb2.wav'
    audioPath = 'C:/temp/archives/c26f727f-3392-4f5b-b331-b4f5f527df97/e68b715c-9954-4ae2-899f-d554f3de5eb2.wav'

    url2 = ipAdress+':45808/upload'
    url3 = ipAdress+':45808/results/'

    files = [('files', (videoName, open(videoPath, 'rb'))),
             ('files',(audioName, open(audioPath, 'rb')))]

    r = requests.post(url2, files=files)

    g = requests.get(url3)

    print(g.status_code)

    if g.status_code == 200:
        result = g.content
        print('Result is: '+result)    


