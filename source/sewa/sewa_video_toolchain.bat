@echo off
call activate sewa
echo Analysing %1...
if not exist %2 (mkdir %2)
python ./sewa_facial_landmark_tracker/sewa_facial_landmark_tracker.py -i %1 -o %2/Facial_Landmarks -f 0.333333
python ./sewa_au_detector/sewa_au_detector.py -i %2/Facial_Landmarks -o %2/AUs.csv
echo All done.
call deactivate sewa
echo on
