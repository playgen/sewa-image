@echo off
call activate sewa
echo Analysing %1...
if not exist %2 (mkdir %2)
python ./sewa_audio_lld_extractor/sewa_audio_lld_extractor.py -i %1 -o %2/LLD.csv
python ./sewa_openxbow/sewa_openxbow.py -i %2/LLD.csv  -o %2/BOAW.libsvm
python ./sewa_val_predictor/sewa_val_predictor.py -i %2/BOAW.libsvm -o %2/VAL.csv
echo All done.
call deactivate sewa
echo on
