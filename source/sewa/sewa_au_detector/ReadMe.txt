Here is the SEWA AU detector

To install the tool, you will need:

1) Install the SEWA facial landmark tracker because you will need it to provide input features.

2) Activate the 'sewa' environment: activate sewa (Windows) or source activate sewa (Ubuntu)

3) scipy: conda install scipy

4) h5py: conda install h5py

5) matplotlib: conda install matplotlib

6) scikit-learn: conda install scikit-learn

7) scikit-image: conda install scikit-image

8) scikit-learn laboratory (SKLL): pip install skll

9) cvxopt: conda install -c omnia cvxopt

10) pystruct: pip install --user --upgrade --no-deps pystruct

11) pandas: conda install pandas

To test the tool, please first run the SEWA facial landmark tracker test script to track the test video clips and then run test_sewa_au_detector.bat (Windows) or test_sewa_au_detector.sh (Ubuntu).
