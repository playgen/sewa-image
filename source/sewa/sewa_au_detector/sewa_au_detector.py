import sys
import getopt
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    from sewa import au_detector


def main(argv):
    input_folder = None
    output_file = None
    opts, args = getopt.getopt(argv, 'i:o:', ['ifolder=', 'ofile='])
    for opt, arg in opts:
        if opt in ("-i", "--ifolder"):
            input_folder = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg
    if input_folder is None:
        print 'Please specify the input PTS folder path!'
    elif output_file is None:
        print 'Please specify the output CSV file path!'
    else:
        print 'Input PTS folder path: ' + input_folder
        print 'Output CSV file path: ' + output_file
        au_detector.basic(input_folder, output_file)
        print 'AU detection completed.'


if __name__ == "__main__":
    main(sys.argv[1:])
