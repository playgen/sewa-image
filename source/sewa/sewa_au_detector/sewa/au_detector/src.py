import cPickle,gzip
import numpy as np
from data_reader import read_all_pts_files
import scipy.ndimage.filters as filters
import os
pwd = os.path.dirname(os.path.abspath(__file__))
# print pwd

model = pwd+'/models/basic01.pklz'
dat = cPickle.load(gzip.open(model,'rb'))


def basic(pwd_in,pwd_out):
    if pwd_in[-1]!='/':pwd_in=pwd_in+'/'
    l49 = read_all_pts_files(pwd_in)

    dat['pip'].rm_seq_mean = 1
    X = dat['pip'].transform([l49])
    y = dat['clf'].predict(X)[0]

    for i in range(y.shape[1]):y[:,i] = filters.gaussian_filter(y[:,i],15)
    res = [range(1, len(l49) + 1)]
    for i in y.T:
        res.append(i > 0.5)
        res.append(i)
    res = np.array(res).T
    np.savetxt(pwd_out, res, fmt=['%d']+['%d','%.6f']*5, delimiter=',', comments='',
               header='Frame_Number,AU01,AU01_Raw,AU02,AU02_Raw,AU04,AU04_Raw,AU12,AU12_Raw,AU17,AU17_Raw')
