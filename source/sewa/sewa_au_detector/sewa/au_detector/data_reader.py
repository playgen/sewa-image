import numpy as np
import glob
import os
import json
import collections
from StringIO import StringIO
from zipfile import ZipFile
from collections import defaultdict


def create_subplot(ax, matrix, x_axis, color_map='default'):
    width = 0.5     
    ind = np.arange(matrix.shape[1])
    if color_map=='default':color_map=cm.Oranges
    bottoms = np.cumsum(np.vstack((np.zeros(matrix.shape[1]), matrix)), axis=0)[:-1]
    for i, row in enumerate(matrix):
        color = color_map(i/float(matrix.shape[0]))
        r = ax.bar(ind, row, width=0.5, color=color, bottom=bottoms[i])
    plt.xticks(ind+width/2., x_axis,fontsize=30)
    plt.xlabel('Action Unit',fontsize = 30)
    return ax


def json_reader(pwd,verbose=0,range=[0,1e100],downsample=1):
    assert(os.path.exists(pwd))

    data = {}
    data['feat'] = defaultdict(list)
    data['labs'] = defaultdict(list)
    data['meta'] = defaultdict(list)
    meta = defaultdict(list)
    
    for k,f in enumerate(glob.glob(pwd+'/*.json')):
        if k<range[0]:continue
        if k>range[1]-1:continue
        
        with open(f) as json_file:
            if verbose:print f

            seq = json.load(json_file)

            idx = np.array(seq['frames'].keys(),dtype=np.uint32)
            idx.sort()
            idx = idx[::downsample]

            for GROUP in seq['frames'][str(idx[0])].keys():
                for key in seq['frames'][str(idx[0])][GROUP]:
                    if key=='image_path':
                        items = np.array([pwd+'/'+seq['frames'][str(i)][GROUP][key] for i in idx])
                    else:
                        items = np.array([seq['frames'][str(i)][GROUP][key] for i in idx])
                    data[GROUP][key].append(items)

            for key in seq['meta']:
                data['meta'][key].append(seq['meta'][key])


    return data


def lm_reader(pwd,verbose=0):
    '''
    '''
    assert(os.path.exists(pwd))

    if os.path.isdir(pwd):
        return _read_all_files(pwd,verbose)
    else:
        if pwd[-3:]=='zip':
            return _read_zip_file(pwd,verbose)
        if pwd[-4:]=='json':
            res = []
            with open(pwd) as data_file:    
                data = json.load(data_file)
                y = data['au_occurence']
                tmp = {}
                for i in data['frames']:
                    key = str(i)
                    while len(key)<10:key='0'+key
                    tmp[key]=data['frames'][i]
                od = collections.OrderedDict(sorted(tmp.items()))
                for i in od:
                    res.append(np.array(tmp[i]).T)
                
            return np.array(res),y
        return _read_file(pwd,verbose)


def _read_file(pwd,verbose=0):
    tmp = []
    with open(pwd) as f:
        for line in f:
            dat = np.genfromtxt(StringIO(line)).astype(np.float32)
            tmp.append(dat)

    pose  = tmp[0]
    lm10 = np.array((tmp[1][0::2],tmp[1][1::2]))
    lm49 = np.array((tmp[2][0::2],tmp[2][1::2]))
    mode  = tmp[3]

    return pose, lm10, lm49, mode

def _read_all_files(pwd,verbose=0):
    pose, lm10, lm49, mode = [], [], [], []

    file_list = list(glob.glob(pwd+'*.txt'))
    file_list.sort()

    for file in file_list:
        if verbose:print file
        p, l10, l49, m = _read_file(file)
        pose.append(p)
        lm10.append(l10)
        lm49.append(l49)
        mode.append(m)

    return np.array(pose), np.array(lm10), np.array(lm49), np.array(mode)

def _read_zip_file(pwd,verbose=0):
    pose, lm10, lm49, mode = [], [], [], []
    with ZipFile(pwd, 'r') as zip:
        for item in zip.namelist():
            f = zip.read(item)
            tmp = []
            for l in StringIO(f):
                tmp.append(np.fromstring(l,sep=' ').astype(np.float32))
            pose.append(tmp[0])
            lm10.append(np.array((tmp[1][0::2],tmp[1][1::2])))
            lm49.append(np.array((tmp[2][0::2],tmp[2][1::2])))
            mode.append(tmp[3])

    return np.array(pose), np.array(lm10), np.array(lm49), np.array(mode)


def read_all_pts_files(pwd, verbose=0):
    lm49 = []

    file_list = list(glob.glob(pwd+'*.pts'))
    file_list.sort()

    for file in file_list:
        if verbose:print file
        lm49.append(_read_pts_file(file))

    return np.array(lm49)


def _read_pts_file(pwd,verbose=0):
    tmp = []
    num_points = 0
    with open(pwd) as f:
        i = 0
        for line in f:
            if i == 1:
                num_points = int(line.split()[1])
            if 3 <= i < 3 + num_points:
                tmp += [float(x) for x in line.split()]
            i += 1
    if num_points == 0:
        tmp = [-1.0] * 98

    return np.array((tmp[0::2], tmp[1::2]))
