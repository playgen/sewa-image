from sklearn import preprocessing
import multiprocessing
import numpy as np
import metrics
import shutil
import os
import glob
import sys
import cPickle,gzip
import pandas as pd
from copy import deepcopy

def subject_independent(sub_idx, folds):
    '''
    '''
    enc = preprocessing.LabelEncoder()
    sub_idx = enc.fit(sub_idx).transform(sub_idx)
    sub_idx = sub_idx % folds

    res = []
    for i in range(folds):
        te = np.argwhere(sub_idx == i)[:, 0]
        tr = np.argwhere(sub_idx != i)[:, 0]
        res.append((tr, te))

    return res

def _run(task):
    '''
    '''
    l, n, clf, c, cv, X, y, save = task

    id = str(np.abs(hash(str(c)+str(X)+str(y))))
    path = save+clf.__class__.__name__+'_'+id+'.pklz'

    if os.path.isfile(path)==True:
        for i,[tr,te] in enumerate(cv):
            pass
            sys.stdout.write("-")
            sys.stdout.flush()
        result = cPickle.load(gzip.open((path)))

    else:
        clf.C = c
        y_hat = [None]*len(y)

        for i,[tr,te] in enumerate(cv):
            X_tr = [X[xx] for xx in tr]
            X_te = [X[xx] for xx in te]
            y_tr = [y[xx] for xx in tr]
            y_te = [y[xx] for xx in te]

            clf.fit(X_tr,y_tr, lock=l )
            for xx in te:
                y_hat[xx] = clf.predict([X[xx]])[0]

            sys.stdout.write("-")
            sys.stdout.flush()

        # save only test data
        idx = np.hstack([xx[1] for xx in cv])
        idx.sort()
        y_hat = [y_hat[xx] for xx in idx]
        y     = [y[xx] for xx in idx]


        result={}
        result['c']     = c
        result['y_lab'] = y
        result['y_hat'] = y_hat
        result['model'] = clf.__class__.__name__

        l.acquire()
        cPickle.dump(result,gzip.open(path,'wb'))
        l.release()

    return result

def predict(X, y, clf, C, cv, n_jobs=1, verbose=1, save='/tmp', metric = metrics.CORR, mode='r',debug=False):
    '''
    <save>
    none: always store predictions in tmp folderand start from scratch
    path: store predictions in path continue if there is something already

    <mode>
    w: delete save folder and start from scratch
    r: search for done results and continue

    <return>
    Tensor: C x F x M
    C is the number of validation parameter, M the number of outputs and
    F the number of frames
    '''
    if save[-1]!='/':save+='/'
    if mode=='w':
        shutil.rmtree(save, ignore_errors=True)
        os.makedirs(save)
    if mode =='r':
        if os.path.isdir(save)==False:
            os.makedirs(save)

    if n_jobs == -1:
        p = multiprocessing.Pool(multiprocessing.cpu_count())
    else:
        p = multiprocessing.Pool(n_jobs)
    m = multiprocessing.Manager()
    l = m.Lock()


    tasks = []
    for n,c in enumerate(C):
        tasks.append([l, n, clf, c, cv, X, y, save])

    toolbar_width = len(tasks)*len(cv)
    sys.stdout.write("[%s]" % (" " * toolbar_width))
    sys.stdout.write("\b" * (toolbar_width+1)) # return to start of line, after '['
    sys.stdout.flush()
    if debug:_run(tasks[0])
    result = p.map(_run, tasks)
    sys.stdout.write("\n")
    p.close()

    out = {}
    for res in result:
        out[str(res['c'])]=res['y_hat']

    y_lab =  result[0]['y_lab']
    y_hat = [out[str(c)] for c in C]




    y_hat_best, c_best, _  = evaluate(y_hat,y_lab,C,metric, verbose=0)
    y_lab = np.vstack(y_lab)

    if verbose>1:

        index = ['MSE','PCC','ICC','F1']
        columns = np.hstack([range(y_hat_best.shape[1]),'AVR.'])

        MSE = -metrics.nMSE(y_hat_best,y_lab)
        MSE = np.hstack((MSE,[MSE.mean()]))
        PCC = metrics.CORR(y_hat_best,y_lab)
        PCC = np.hstack((PCC,[PCC.mean()]))
        ICC = metrics.ICC(y_hat_best,y_lab)
        ICC = np.hstack((ICC,[ICC.mean()]))
        F1 = metrics.F1_detection(y_hat_best!=0,y_lab!=0)
        F1 = np.hstack((F1,[F1.mean()]))
        dat = np.vstack((MSE,PCC,ICC,F1))
        pd.set_option('display.float_format', lambda x: '%.2f' % x)
        tab = pd.DataFrame(dat,index=index, columns = columns)
        print tab
        print 'C: ',c_best

    if verbose>2:
        print 
        columns = ['MSE','PCC','ICC','F1']
        index = []
        MSE,PCC,ICC,F1 = [],[],[],[]
        for res in result:
            tmp = np.vstack(res['y_hat'])
            y_lab = np.vstack(y_lab)
            MSE.append(-metrics.nMSE(tmp,y_lab).mean())
            ICC.append(metrics.ICC(tmp,y_lab).mean())
            PCC.append(metrics.CORR(tmp,y_lab).mean())
            F1.append(metrics.F1_detection(tmp,y_lab).mean())
            index.append(str(res['c']))
        dat = np.vstack((MSE,PCC,ICC,F1)).T
        tab = pd.DataFrame(dat,index=index, columns = columns)
        print tab

    return out[str(c_best)], c_best

def evaluate(Y_HAT , y, C, metric=metrics.CORR, verbose=1):
    '''
    '''
    y = np.vstack(y)
    Y_HAT = [np.vstack(xx) for xx in Y_HAT]


    score = np.array([metric(i,y) for i in Y_HAT])
    if len(score.shape)==1:score=score[:,None]
    idx = np.argmax(score.mean(1))

    x = np.argmax(score,0)
    y = np.arange(score.shape[1])
    score = np.vstack((score,score[x,y,None].T))
    score = np.vstack((score,score[idx,None]))

    dat = np.hstack((score,score.mean(1)[:,None]))



    index = [str(i) for i in C] 
    index.append('Best Combi.')
    index.append(str(C[idx]))

    columns = np.hstack([range(score.shape[1]),'AVR.'])

    pd.set_option('display.float_format', lambda x: '%.2f' % x)
    tab = pd.DataFrame(dat,index=index, columns = columns)

    if verbose>0:print tab


    return Y_HAT[idx], C[idx], tab

def get_results(path,metric=metrics.CORR,verbose=0):
    y_hat,y_lab,C = [],[],[]
    for f in glob.glob(path+'/*.pklz'):
        res = cPickle.load(gzip.open(f))
        y_hat.append(res['y_hat'])
        y_lab.append(res['y_lab'])
        C.append(res['c'])

    y = y_lab[0]
    y_hat_best, c_best, _  = evaluate(y_hat,y_lab[0],C,metric, verbose=0)
    index = ['MSE','PCC','ICC','F1']
    columns = np.hstack([range(y_hat_best.shape[1]),'AVR.'])

    if verbose>0:
        MSE = -metrics.nMSE(y_hat_best,y)
        MSE = np.hstack((MSE,[MSE.mean()]))
        PCC = metrics.CORR(y_hat_best,y)
        PCC = np.hstack((PCC,[PCC.mean()]))
        ICC = metrics.ICC(y_hat_best,y)
        ICC = np.hstack((ICC,[ICC.mean()]))
        F1 = metrics.F1(y_hat_best!=0,y!=0)
        F1 = np.hstack((F1,[F1.mean()]))
        dat = np.vstack((MSE,PCC,ICC,F1))
        pd.set_option('display.float_format', lambda x: '%.2f' % x)
        tab = pd.DataFrame(dat,index=index, columns = columns)

        print res['model']
        print tab
        print 'C: ',c_best

    if verbose>1:
        print 
        columns = ['MSE','PCC','ICC','F1']
        index = []
        MSE,PCC,ICC,F1 = [],[],[],[]
        for f in glob.glob(path+'/*.pklz'):
            res = cPickle.load(gzip.open(f))
            MSE.append(-metrics.nMSE(res['y_hat'],y).mean())
            ICC.append(metrics.ICC(res['y_hat'],y).mean())
            PCC.append(metrics.CORR(res['y_hat'],y).mean())
            F1.append(metrics.F1(res['y_hat'],y).mean())
            index.append(res['c'])
        dat = np.vstack((MSE,PCC,ICC,F1)).T
        tab = pd.DataFrame(dat,index=index, columns = columns)
        print tab

    return y_hat_best, y
