import numpy as np
from sklearn import preprocessing

def _con_probAB(y0,y1):
    # conditional probability of y0=e1 and y1=e2
    # given: e1 or e2 is true
    res = np.zeros([y0.shape[1],y1.shape[1]])
    for i in range(y0.shape[1]):
        for j in range(y1.shape[1]):
            k = y0[:,i]
            l = y1[:,j]
            IN = np.sum((k==1)*(l==1))
            ALL = np.sum(((k==1)+(l==1)>0))
            res[i,j]=IN/np.float64(ALL)
    return res

def _hist2d(y0,y1,normalized=True):
    y0 = np.int32(y0)
    y1 = np.int32(y1)
    res = np.dot(y0.T,y1)
    if normalized:res = res/np.float64(np.sum(res))
    return res


def cov(y):
    return np.cov(y.T)

def cor(y):
    return np.corrcoef(y.T)

def cooc(y):
    return _con_probAB(y>0,y>0)

def hist2d(y):
    return _hist2d(y>0,y>0)

def hist2dI(y):
    lb = preprocessing.LabelBinarizer()
    lb.fit(y.flatten())
    res = {}
    for i in range(y.shape[1]):
        for j in range(y.shape[1]):
            k = lb.transform(y[:,i])
            l = lb.transform(y[:,j])
            res[i,j] = _hist2d(k,l)
    return res

def coocI(y):
    lb = preprocessing.LabelBinarizer()
    lb.fit(y.flatten())
    res = {}
    for i in range(y.shape[1]):
        for j in range(y.shape[1]):
            k = lb.transform(y[:,i])
            l = lb.transform(y[:,j])
            res[i,j] = _con_probAB(k,l)
    return res
