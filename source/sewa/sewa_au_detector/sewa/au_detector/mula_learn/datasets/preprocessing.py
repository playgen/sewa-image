from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from skimage import transform as tf
import numpy as np
import matplotlib.pyplot as plt
import cPickle,gzip

def compute_features(data, n_components=1e100):
    '''
    '''
    data = remove_invalid_samples(data)
    data = affine_transform_to_mean(data)
    data = lm_2_X(data,n_components)
    #cPickle.dump(data,gzip.open('/tmp/data.pklz','wb'))
    #data = cPickle.load(gzip.open('/tmp/data.pklz','rb'))
    X, y, S, id = make_static(data)
    return X,y,S

def remove_invalid_samples(data):

    res = {}
    for id in data:

        au_int = data[id]['au_int']
        lm = data[id]['lm']
        S = data[id]['S']

        if (au_int.shape[0]!=lm.shape[0]) or (au_int.shape[0]!=lm.shape[0]):
            print 'shape error: remove entire sequencd', id
            continue

        #find invalid labels
        tmp = np.zeros_like(au_int)
        for i in [0,1,2,3,4,5]:tmp[au_int==i]=1
        idx_correct = np.all(tmp==1,1)

        tmp = {}
        tmp['au_int']=au_int[idx_correct]
        tmp['lm']=lm[idx_correct]
        tmp['S']=S
        res[id]=tmp

    return res

def make_static(data):

    X,y,S,ID = [],[],[],[]

    subjects = []
    for id in data:subjects.append(data[id]['S'])
    unique_subjects = np.unique(subjects)

    for id in data:
        print id
        S_id = np.argwhere(unique_subjects==data[id]['S'])[0][0]
        S.append(np.ones(data[id]['X'].shape[0])*S_id)
        X.append(data[id]['X'])
        y.append(data[id]['au_int'])
        ID.append((id,)*data[id]['X'].shape[0])

    X = np.vstack(X)
    y = np.vstack(y).astype(np.int16)
    S = np.hstack(S)
    ID = np.hstack(ID)

    return X,y,S,ID

def affine_transform_to_mean(data):

    X = []
    for id in data:X.append(data[id]['lm'])
    X = np.concatenate(X,axis=0)
    mean_face = X.mean(0)
    sp = np.array([20, 23, 26, 29, 11, 12, 13, 14, 15, 16, 17, 18, 19])-1
    #sp = np.arange(49)

    for id in data:
        print id
        lm = data[id]['lm']
        for frame in range(lm.shape[0]):
            tform = tf.estimate_transform('affine', lm[frame,:,sp],mean_face[:,sp].T)
            lm[frame] = tform(lm[frame].T).T
        data[id]['lm']=lm

    return data

    '''
    '''

def lm_2_X(data,n_components):

    # remove nan
    tmp = {}
    for id in data:
        if np.isnan(data[id]['lm'].sum()):continue
        if data[id]['lm'].shape[0]==0:continue
        tmp[id]=data[id]
    data=tmp

    # compute_features vector
    for id in data:
        lm = data[id]['lm']
        data[id]['X']=np.hstack((lm[:,0,:],lm[:,1,:]))

    # remove sequence mean
    for id in data:
        data[id]['X']=data[id]['X']-data[id]['X'].mean(0)

    # StandardScaler
    X = []
    for id in data:X.append(data[id]['X'])
    X = np.vstack(X)
    sds = StandardScaler()
    sds.fit(X)
    for id in data:data[id]['X'] = sds.transform(data[id]['X'])

    # compute pca
    X = []
    for id in data:X.append(data[id]['X'])
    X = np.vstack(X)

    pca = PCA(n_components=n_components)
    pca.fit(X)
    for id in data:data[id]['X'] = pca.transform(data[id]['X'])

    # StandardScaler
    X = []
    for id in data:X.append(data[id]['X'])
    X = np.vstack(X)
    sds = StandardScaler()
    sds.fit(X)
    for id in data:data[id]['X'] = sds.transform(data[id]['X'])

    return data

