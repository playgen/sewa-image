import cPickle
import gzip
import os
module_path = os.path.dirname(__file__)

def load_pain():
    data = cPickle.load(gzip.open(module_path+'/pain_subset.pklz','rb'))
    info = {}
    info['y_name'] = [4,6,7,9,10,12,20,25,26,43]
    info['name'] = 'pain'
    return data['X'],data['y'],data['S'], info
