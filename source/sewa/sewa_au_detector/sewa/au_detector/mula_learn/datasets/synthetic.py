import numpy as np
import Multivariate_Sampling.copulas as copulas
import Multivariate_Sampling.margins as margins

def load_synthetic(N=1000, I=2, seed=1, sigma=1.5, theta=8, v0_shift=[0,0]):

    np.random.seed(seed=seed)


    C = copulas.Frank(theta)

    v0 = np.linspace(-I,I,I)

    F1 = {}
    F2 = {}
    for y1 in range(I):
        for y2 in range(I):
            v0_1 = v0[y1]+v0_shift[0]
            v0_2 = v0[y2]+v0_shift[1]
            F1[y1,y2] = margins.sigmoid(v0_1,sigma)
            F2[y1,y2] = margins.sigmoid(v0_2,sigma*2)
            print '...'

    y = np.random.randint(0,I,(N,2))

    X = []
    for i in range(N):

        y1, y2 = y[i]

        u1, v2 = np.random.uniform(size=2)
        u2 = C(u1, v2)
        Z1 = F1[y1,y2](u1)
        Z2 = F2[y1,y2](u2)

        X.append([ Z1, Z2 ])


    return np.array(X), y, None, None


if __name__ == "__main__":

    import matplotlib.pyplot as plt
    X,y = load_synthetic()
    plt.scatter( X[:,0], X[:,1], c=y[:,0]+y[:,1]*2 )
    plt.show()
