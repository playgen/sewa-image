import cPickle
import gzip
import os
module_path = os.path.dirname(__file__)

def load_fera2015():
    data = cPickle.load(gzip.open(module_path+'/fera2015_subset.pklz','rb'))
    info = {}
    info['y_name'] = [6,10,12,14,17]
    info['name'] = 'fera'
    return data['X'],data['y'],data['S'], info
