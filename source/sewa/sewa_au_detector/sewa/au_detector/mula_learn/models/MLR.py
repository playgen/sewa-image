import numpy as np
import theano as T
import theano.tensor as TT
import TeaSpoon
import cPickle,gzip


class MLR():

    def __init__(
            self,
            X,
            y,
            C=1e3,
            max_iter=100,
            verbose=0,
            digitize = 0,
    ):
        """
        """
        X = np.vstack(X)
        y = np.vstack(y)

        self.C = C
        self.max_iter = max_iter
        self.verbose = verbose
        self.digitize = digitize
        self.p0 = self._init_para(X, y)
        self.Output = np.unique(y)
        self.p1 = self.p0
        self.C_default = np.power(10.,np.arange(-5,6))
    
        # dry-run
        TeaSpoon.debug(self._loss, [self.p0, X, y])
        TeaSpoon.debug(self._predict, [self.p0, X])

    def _init_para(self, X, y):
        '''
        '''
        np.random.seed(1)
        F, C, M = X.shape[1], len(np.unique(y)), y.shape[1]

        w = np.random.rand(M, F)-0.5
        b = np.zeros(M)

        para = {}
        para['b'] = TeaSpoon.parameter(b)
        para['w'] = TeaSpoon.parameter(w)

        return para


    def _predict(self, para, X):
        """
        """
        w = para['w'].value
        b = para['b'].value
        r = TT.dot(X,w.T)+b
        return r

    def _loss(self, para, X, y):
        w = para['w'].value
        b = para['b'].value
        r = TT.dot(X,w.T)+b
        # loss  = 1./self.C + 
        loss = TT.mean(TT.sqr(y-r))
        loss += 1./self.C * TT.sum(TT.sqr(para['w'].value))
        return loss

    def fit(self, X, y, lock=None):
        X = np.vstack(X)
        y = np.vstack(y)
        """

        """

        if lock!=None:lock.acquire()
        self.predict_C = TeaSpoon.compile(
            self._predict, [self.p0, X], jac=False)

        self.loss_C, self.loss_grad_C = TeaSpoon.compile(
            self._loss, [self.p0, X, y], jac=True)
        if lock!=None:lock.release()

        if self.verbose == 0:callback = None
        if self.verbose >= 1:callback = 'default'

        # start optimization
        self.p1, self.cost = TeaSpoon.optimize(
            fun=self.loss_C,
            p0=self.p0,
            jac=self.loss_grad_C,
            callback=callback,
            method='CG',
            args=(X, y),
            options = {'maxiter': self.max_iter, 'disp': self.verbose > 1}
        )
        return self

    def __predict(self, X):
        """
        """
        para = TeaSpoon.utils.para_2_vector(self.p1)
        y_hat = self.predict_C(para, X)
        if self.digitize:
            y_hat = np.digitize(y_hat,self.Output)
        return y_hat

    def predict(self,X):
        return [self.__predict(ii) for ii in X]
