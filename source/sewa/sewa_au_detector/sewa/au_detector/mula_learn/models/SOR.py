import numpy as np
import theano as T
import theano.tensor as TT
import TeaSpoon
import utils
import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'utils')))
import metrics


class SOR():

    def __init__(
            self,X,y,
            C=[0],
            max_iter=500,
            margins='normcdf',
            verbose=0,
            loss_function = 'ncll',
            output = 'MAP',
            debug = 0,
            seed = 1,
            ):
        """
        """
        X = np.vstack(X)
        y = np.vstack(y)
        self.C = C
        self.C_default = [0.]
        self.max_iter = max_iter
        self.verbose = verbose
        self.seed = seed
        self.loss_function = loss_function
        self.ouput = output

        self.p0, self.shape = self._init_para(X, y)
        self.p1 = self.p0

        if margins=='sigmoid':self._margin = utils.margins.sigmoid
        if margins=='normcdf':self._margin = utils.margins.normcdf

        # dry-run
        if debug==True:
            TeaSpoon.debug(self._pdf,  [self.p0, X])
            TeaSpoon.debug(self._loss, [self.p0, X, y])
            TeaSpoon.debug(self._predict, [self.p0, X])

    def _init_para(self, X, y):
        '''
        '''
        np.random.seed(self.seed)

        F =  X.shape[1]
        C = len(np.unique(y))
        M = y.shape[1]

        w = np.random.uniform(-1,1,(M, F))
        d = np.ones((M, C-2))

        # marginal function parameter
        b = np.ones(M) *(1 - 0.5 * C)
        s = np.ones(M)

        para = {}
        para['b'] = TeaSpoon.parameter(b)
        para['w'] = TeaSpoon.parameter(w)
        para['d'] = TeaSpoon.parameter(d)
        para['s'] = TeaSpoon.parameter(s)

        return para, [F,C,M]
    
    def _cdf(self, para, X):
        '''
        '''
        w = para['w'].value
        b = para['b'].value
        d = para['d'].value
        s = para['s'].value
        b = b.dimshuffle(0, 'x')

        NU = TT.extra_ops.cumsum(
            TT.concatenate((b, TT.sqr(d)), axis=1),
            axis=1)

        NU = TT.concatenate(
            (-1e20 * TT.ones_like(b), NU, 1e20 * TT.ones_like(b)),
            axis=1)

        NU = NU.dimshuffle('x', 0, 1)
        Z = T.dot(w, X.T).dimshuffle(1, 0, 'x')
        Z = TT.extra_ops.repeat(Z, NU.shape[2], 2)
        S = s.dimshuffle('x', 0, 'x')

        cdf = self._margin(NU, TT.sqr(S), Z)

        return cdf 

    def _pdf(self, para, X):
        cdf = self._cdf(para, X)
        pdf = cdf[:, :, :-1]-cdf[:, :, 1:]
        return pdf

    def _predict(self, para, X):
        P = self._pdf(para, X)

        if self.ouput=='expectation':
            return utils.statistics.expectation(P)
        if self.ouput=='MAP':
            return TT.argmax(P, 2)
        if self.ouput=='probability':
            return P

    def _loss(self, para, X, y):
        '''
        '''
        P = self._pdf(para, X)

        if self.loss_function=='mse':
            E = utils.statistics.expectation(P)
            loss = TT.mean( TT.sqr(E-y) )

        if self.loss_function=='ncll':
            CLL = utils.statistics.compute_cll(P, y)
            loss = -TT.mean( CLL )
        return loss

    def fit(self, X, y, X_te=None,y_te=None,lock=None):
        X = np.vstack(X)
        y = np.vstack(y)
        """
        """
        # if self.weights==None:self.w = 1/X.shape[0]
        # self.w = utils.statistics.weights(y,self.weights)
        if lock!=None:lock.acquire()
        self._predict_T = TeaSpoon.compile(self._predict, [self.p0, X], jac=False)
        self._loss_T, self._grad_T = TeaSpoon.compile(self._loss, [self.p0, X, y], jac=True)
        if lock!=None:lock.release()


        def callback(pi):
            y_hat = TeaSpoon.exe(self._predict_T, [pi, X])
            out = {
            'ICC_tr': metrics.ICC(y_hat,y).mean(),
            'Loss'  : TeaSpoon.exe(self._loss_T, [pi, X,y]),
            }
            if X_te!=None:
                y_hat = TeaSpoon.exe(self._predict_T, [pi, X_te])
                out['ICC_te'] =  metrics.ICC(y_hat,y_te).mean()

            opt = {'freq':5}
            return out, opt

        if self.verbose == 0:callback = None

        # start optimization
        self.p1, self.cost = TeaSpoon.optimize(
            fun=self._loss_T,
            p0=self.p0,
            jac=self._grad_T,
            callback=callback,
            method='CG',
            args=(X, y),
            options = {'maxiter': self.max_iter, 'disp': self.verbose > 1}
        )
        return self

    def predict(self, X):
        Y = [TeaSpoon.exe(self._predict_T, [self.p1, ii]) for ii in X]
        return Y

# loss += 1./self.C[0] * TT.sum(TT.sqr(para['w'].value))
# loss += 1./self.C[1] * TT.sum(TT.sqr(TT.log(TT.sqr(para['s'].value))))
