from BASE import BASE 
from sklearn.gaussian_process import GaussianProcess
import numpy as np
from sklearn.preprocessing import normalize

class GP(BASE):

    def __init__(self,X, y, C=1e-3, max_size=2500, **kwargs):
        BASE.__init__(self, X, y, **kwargs)
        self.C_default = [0]
        self.type='regression'
        self.max_size=max_size
        self.C = C

    def fit(self, X, y, lock=None):
        """
        """
        X = np.vstack(X)
        y = np.vstack(y)
        if X.shape[0]>self.max_size:
            i = np.random.permutation(X.shape[0])[:self.max_size]
            X = X[i]
            y = y[i]

        X = normalize(X + np.var(X)*np.random.rand(*X.shape)*1e-6)
        X = normalize(X)
        # y = y/6.

        self.clf = GaussianProcess(
                corr='squared_exponential',
                verbose = self.verbose,
                nugget = 1e-1*np.var(y),
                theta0 = np.max(X)-np.min(X),
                normalize = False,
                )
        self.clf.fit(X, y)

        return self

    def predict(self, X):
        return [self.clf.predict(normalize(xx)) for xx in X]
