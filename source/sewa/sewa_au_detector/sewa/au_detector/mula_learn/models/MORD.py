from BASE import BASE 
from BASE import MultiOutputTransform
import mord
import numpy as np

class MORD(BASE):

    def __init__(self,X,y, C=1., **kwargs):
        BASE.__init__(self, X, y, **kwargs)
        self.C_default = np.power(10.,np.arange(-3,4))
        self.C_default = np.hstack((self.C_default,[0]))
        self.type='classification'
        self.C = C

    def fit(self, X, y, lock=None):
        """
        """
        X = np.vstack(X)
        y = np.vstack(y)

        y = MultiOutputTransform(self.enc, y)

        self.MO_clf = []

        for y_ in y.T:
            clf = mord.LogisticAT(
                alpha=self.C,
            )
            clf.fit(X, y_)
            self.MO_clf.append(clf)

        return self
