from BASE import BASE 
from BASE import MultiOutputTransform
from sklearn.svm import SVR as SVR_sklearn
from itertools import combinations
import numpy as np

class SVR(BASE):

    def __init__(self,X,y, C=[1.,1.], kernel='rbf', **kwargs):
        BASE.__init__(self, X, y, **kwargs)
        self.type='regression'
        self.kernel = kernel 
        self.C_default = []
        if self.kernel=='linear':
            self.C_default=np.power(10.,np.arange(-5,6))
        else:
            for a in np.power(10.,np.arange(-3,4)):
                for gamma in np.power(10.,np.arange(-3,4)):
                    self.C_default.append([a,gamma])
        self.C = C

    def fit(self, X, y, lock=None):
        """
        """

        X = np.vstack(X)
        y = np.vstack(y)
        y = MultiOutputTransform(self.enc, y)

        self.MO_clf = []

        if self.kernel=='linear':
            for y_ in y.T:
                clf = SVR_sklearn(
                    C=self.C,
                    # gamma=self.C[1],
                    verbose=self.verbose,
                    max_iter = self.max_iter,
                    kernel=self.kernel,
                )
                clf.fit(X, y_)
                self.MO_clf.append(clf)
        else:
            for y_ in y.T:
                clf = SVR_sklearn(
                    C=self.C[0],
                    gamma=self.C[1],
                    verbose=self.verbose,
                    max_iter = self.max_iter,
                    kernel=self.kernel,
                )
                clf.fit(X, y_)
                self.MO_clf.append(clf)

        return self
