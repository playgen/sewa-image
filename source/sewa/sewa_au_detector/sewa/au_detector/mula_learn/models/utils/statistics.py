import theano.tensor as TT
import theano as T
import numpy as np
from sklearn import preprocessing

def weights(y,type):
    if type==None:
        return np.ones_like(y)/np.float64(y.shape[0])
    if type=='balanced':
        lb = preprocessing.LabelBinarizer()
        lb.fit(y.flatten())
        res = np.zeros_like(y)
        for i in range(y.shape[1]):
            w_ = np.sum(lb.transform(y[:,i]),0)
            res[:,i] = w_[y[:,i]]
        return (np.float64(res)**-1.)


def expectation(P):
    states = TT.arange(P.shape[2]).dimshuffle('x','x',0)
    states = TT.extra_ops.repeat(states,P.shape[0],0)
    states = TT.extra_ops.repeat(states,P.shape[1],1)
    return TT.sum(P*states,axis=2)

def log_prob(P, realmin = 1e-20):
    '''
    numerical stabil log function
    for some reason theano needs a high realmin value
    '''

    # if prob is less than realmin, set it to realmin
    idx = TT.le(P,realmin).nonzero()
    P = TT.set_subtensor(P[idx],realmin)

    # if prob larger than 1-realmin, set it to 1-realmin
    idx = TT.ge(P,1-realmin).nonzero()
    P = TT.set_subtensor(P[idx],1-realmin)

    idx = TT.isnan(P).nonzero()
    P = TT.set_subtensor(P[idx],realmin)

    idx = TT.isinf(P).nonzero()
    P = TT.set_subtensor(P[idx],realmin)

    return TT.log(P)

def c_frank(self, u,v,d):
    '''
    Frank Copula
    '''
    d = self.theta_range*(TT.nnet.sigmoid(d)-0.5)
    U = TT.exp(-d*u)-1
    V = TT.exp(-d*v)-1
    D = TT.exp(-d  )-1
    C = 1+U*V/D

    idx = TT.le(C,0).nonzero()
    C = TT.set_subtensor(C[idx],0)

    C = -1/(d) * TT.log(C)

    return C

def c_gumbel(u,v,d):
    '''
    Gumbel Copula
    '''

    d = 26*(TT.nnet.sigmoid(d))-1
    U = (-TT.log(u))**d
    V = (-TT.log(v))**d
    P = TT.exp(-((U+V)**(1/d)))

    idx = TT.le(P,0).nonzero()
    P = TT.set_subtensor(P[idx],0)

    idx = TT.ge(P,1).nonzero()
    P = TT.set_subtensor(P[idx],1)

    return P

def c_indep(self, u,v,d=None):
    '''
    '''
    return u*v

def compute_cll(pdf, y=None):
    '''
    pdf:     [AU X Frame X Label]

    node that there are M+1 Thresholds and they have to go from 0 to 1
    '''
    if y:
        y_ = y.T.astype('int8').flatten(1)
        pdf = pdf.T.flatten(2)
        idx = TT.arange(y_.shape[0])
        P = pdf[y_,idx]
        P = P.reshape(y.shape)
    else:
        P = pdf

    return log_prob(P)

def compute_cjll(self, pdf, theta, edges,Y=None):
    '''
    '''
    cdf = TT.extra_ops.cumsum(pdf,axis=2)
    cdf = TT.concatenate((TT.zeros_like(cdf[:,:,[0]]),cdf),axis=2)
    if self.copula=='frank':copula = c_frank
    if self.copula=='indep':copula = c_indep

    def comp_jpdf(cdf, d, y=None):
        '''
        cdf : list of cdfs              [cdf_1, cdf_2]
        y : list of vecotr of labels    [y_1, y_1]
        '''
        idx = TT.arange(cdf.shape[1])
        if y:
            u_0 = cdf[0,idx,y[0]]
            u_1 = cdf[0,idx,y[0]+1]
            v_0 = cdf[1,idx,y[1]]
            v_1 = cdf[1,idx,y[1]+1]

            P =  copula(self, u_0,v_0,d)
            P -= copula(self, u_0,v_1,d)
            P -= copula(self, u_1,v_0,d)
            P += copula(self, u_1,v_1,d)
        else:

            #add a dimension to each pdf and repeat values
            cdf_0 = TT.extra_ops.repeat(cdf[0].dimshuffle(0,1,'x'),cdf[1].shape[1],2)
            cdf_1 = TT.extra_ops.repeat(cdf[1].dimshuffle(0,'x',1),cdf[0].shape[1],1)

            if self.association=='feature_dependent':
                d = d.dimshuffle(0,'x','x')
                d = TT.extra_ops.repeat(d,cdf_0.shape[1],1)
                d = TT.extra_ops.repeat(d,cdf_1.shape[1],2)

            j_cdf = copula(self, cdf_0,cdf_1,d)
            P = j_cdf[:,1:,1:] + j_cdf[:,:-1,:-1] - j_cdf[:,:-1,1:] - j_cdf[:,1:,:-1]
        return P


    cdf = cdf.dimshuffle(1,0,2)
    if Y != None:
        Y = Y.T.astype('int8')

    def inner_function(edges,theta,cdf):
        if Y == None:
            jpdf = comp_jpdf(cdf[edges], theta)
        else:
            jpdf = comp_jpdf(cdf[edges], theta, Y[edges])
        return jpdf

    jpdf , _ = T.scan(
        fn=inner_function,
        sequences=[edges, theta],
        non_sequences=[cdf]
    )

    #jpdf = inner_function(edges[0],theta[0],cdf)



    return log_prob(jpdf)

