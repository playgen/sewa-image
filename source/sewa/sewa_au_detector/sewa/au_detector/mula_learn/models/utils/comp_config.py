import numpy as np
import itertools
np.random.seed(1)

def comp_config(shape,y):

    F, C, M = shape

    y_star, y_map = [], {}
    for i,y_ in enumerate(itertools.product(range(C), repeat=M)):
        y_star.append(y_)
        y_map[str(np.array(y_))]=i

    y_star = np.array(y_star,dtype=np.uint8)
    i = np.array([y_map[str(x)] for x in y])

    return y_star, i

if __name__ == '__main__':

    y = np.random.randint(0,2,(100,6))
    # y_star, i = comp_config(y)
    # assert np.all(y_star[i]==y)
