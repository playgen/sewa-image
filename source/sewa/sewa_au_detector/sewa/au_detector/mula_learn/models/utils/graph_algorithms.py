from sklearn.metrics import mutual_info_score
from scipy.sparse.csgraph import minimum_spanning_tree
import itertools
from copy import copy
import numpy as np
import networkx as nx

def _compute_mutural_info(y):
    res = np.zeros((y.shape[1],y.shape[1]))
    for i, y1 in enumerate(y.T):
        for j, y2 in enumerate(y.T):
            if i<=j:continue
            mi = mutual_info_score(y1,y2)
            res[i,j]= mi
            res[j,i]= mi
    return res

def _comput_mst(mat):
    mst = minimum_spanning_tree(mat).toarray()
    E0,E1 = mst.nonzero()
    E,C = [],[]
    for e0, e1 in zip(E0,E1):
        E.append((e0,e1))
        C.append(mat[e0,e1])
    return np.array(E,dtype=int),np.array(C) 

def _get_size(tree):
    edges = tree[0]
    e0 = edges[:,0]
    e1 = edges[:,1]
    nodes = np.union1d(np.unique(e0),np.unique(e1))
    return len(nodes)

def _split(tree):
    Edges,Cost = tree
    i = np.argmax(Cost)
    s0,s1 = Edges[i]
    Edges[i]=[-1,-1]
    E0 = []
    print Edges

    print s0,s1


# networkx graph split
###################################################

def _rm_min_edge(G):
    E,W=[],[]
    for e in G.edges():
        W.append(G.get_edge_data(*e)['weight'])
        E.append(e)
    G.remove_edge(*E[np.argmin(W)])

def _transfer_nodes(from_g,to_g):
    '''
    check if to_g already contains nodes. 
    if yes, do not transfer them 
    '''
    for n in from_g.nodes():
        for d in to_g:
            for dn in d.nodes():
                if dn==n:
                    return 0
    to_g.append(from_g)
    return to_g 

def _fully_connect(d,G_old):
    D = nx.Graph()
    D.add_nodes_from(d)
    for n1 in D.nodes():
        for n2 in D.nodes():
            if n1>n2:
                w = G_old.get_edge_data(n1,n2)['weight']
                D.add_edge(n1,n2, weight=w)
    return D

def _cut_graph(G,max_number_of_nodes=5):
    '''
    '''
    G_old=G.copy()
    Done = []
    for i in range(G.number_of_edges()):
        _rm_min_edge(G)

        # find subgraph
        for g in nx.connected_component_subgraphs(G):
            if g.number_of_nodes()<=max_number_of_nodes:
                _transfer_nodes(g,Done)

    Done_fully = []
    for d in Done:
        Done_fully.append(_fully_connect(d,G_old))

    res = nx.Graph()
    for d in Done_fully:res = nx.compose(res,d)

    return res

def _construct_graph(y):

    G=nx.Graph()

    for n in range(y.shape[1]):
        G.add_node(n)

    for n1 in range(y.shape[1]):
        for n2 in range(y.shape[1]):
            if n1>=n2:continue
            G.add_edge(n1,n2, weight=y[n1,n2])
    
    return G

def _get_struct(G):
    '''
    '''
    E,N = [],[]
    for g in nx.connected_component_subgraphs(G):
        E_,N_ = [],[]
        for e in g.edges():E_.append(e)
        for n in g.nodes():N_.append(n)
        E.append(np.array(E_,dtype=np.uint16))
        N.append(np.array(N_,dtype=np.uint16))
    return N,E

###################################################


def fully_connected_graph(y):
    edges = []
    for e_ in itertools.combinations(range(y.shape[1]),2):edges.append(e_)
    return np.array(edges)

def chow_liu(y):
    mi = _compute_mutural_info(y)
    mst = minimum_spanning_tree(1/mi).toarray()
    edges = np.vstack(mst.nonzero()).T
    return edges

def split(y,max_nodes=3):
    mi = _compute_mutural_info(y)
    G = _construct_graph(mi)
    G = _cut_graph(G,max_nodes)
    N,E = _get_struct(G)
    return N,E
