import theano.tensor as TT

def select2d(array_3d, index_2d):

    array_3d_= array_3d.flatten(2)
    index_2d_ = index_2d.flatten(1)
    idx = TT.arange(index_2d_.shape[0])

    array_3d_ = array_3d_[index_2d_.astype('int16'),idx]
    return array_3d_.reshape(index_2d.shape)
