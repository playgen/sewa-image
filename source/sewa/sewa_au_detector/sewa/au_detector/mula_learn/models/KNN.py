from BASE import BASE 
from BASE import MultiOutputTransform
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
import numpy as np

class KNN(BASE):

    def __init__(self,X,y, C=5, **kwargs):
        BASE.__init__(self, X, y, **kwargs)
        self.C_default = np.power(2,np.arange(8))
        self.C = C

        self.type='classification'

    def fit(self, X, y, lock=None):
        """
        """

        X = np.vstack(X)
        y = np.vstack(y)
        self.clf = KNeighborsClassifier(
                n_neighbors = self.C,
                )
        self.clf.fit(X, y)

        return self

    def predict(self, X):
        return [self.clf.predict(xx) for xx in X]
