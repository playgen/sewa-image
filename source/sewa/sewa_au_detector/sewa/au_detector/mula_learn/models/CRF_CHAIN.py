from BASE import BASE 
import numpy as np
import numpy as np
from pystruct.datasets import load_letters
from pystruct.models import ChainCRF
from pystruct.learners import *
from sklearn import preprocessing
le = preprocessing.LabelEncoder()


class CRF_CHAIN(BASE):

    def __init__(self,X,y, C=1., class_weight=None, **kwargs):
        y = np.vstack(y)
        X = np.vstack(X)
        BASE.__init__(self, X, y, **kwargs)
        self.type='classification'
        self.M = y.shape[1]
        self.C_default = np.power(10.,np.arange(-3,4))
        self.class_weight=class_weight
        self.C = C

        self.clf = {}


    def fit(self, X, y, lock=None):

        for i in range(self.M):
            self.clf[i] = FrankWolfeSSVM(
                    model = ChainCRF(),
                    C=self.C, 
                    max_iter=500
                    )


            y_ = [xx[:,i] for xx in y]
            y_ = [le.fit_transform(xx) for xx in y_]
            self.clf[i].fit(X, y_)

        return self

    def predict(self,X):
        tmp = [self.clf[xx].predict(X) for xx in range(self.M)]
        tmp = zip(*tmp)
        y = [np.vstack(xx).T for xx in tmp]
        return y
