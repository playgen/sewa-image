import numpy as np
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.filters import median_filter 
import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'utils')))
import metrics



class predictions():
    '''
    '''
    def __init__(self, smooth=0 , shift=0 , threshold=0.5):
        '''
        '''
        self.filter = gaussian_filter 
        self.smooth=smooth
        self.shift=shift
        self.threshold=threshold

    def fit(self,y,y_hat):

        y1_ = np.vstack(y).T
        y2_ = np.vstack(y_hat).T

        res = []
        for th in np.linspace(0,1,40):
            for s in np.arange(0,10)**2:
                tmp = np.array([self.filter(ii,s) for ii in y2_])
                tmp = tmp>th
                score = metrics.CORR(y1_.T,tmp.T)[0]
                res.append([score,s,th])

        res = np.array(res)
        i = np.argmax(res[:,0])
        score, self.smooth, self.threshold = res[i]
        print self.smooth,self.threshold
        return self

    def transform(self,y):
        y = [self.filter(ii[:,0],self.smooth)[:,None] for ii in y]
        y = [ii>self.threshold for ii in y]
        return y
