from sklearn.decomposition import PCA
from sklearn.preprocessing import Imputer
from skimage import transform as tf
from copy import copy,deepcopy
import numpy as np
import cPickle,gzip
from scipy.signal import medfilt
import os
pwd = os.path.dirname(os.path.abspath(__file__))

imputer = Imputer(strategy='median')


class process_data():
    '''
    '''
    def _affine_transform(self,lm):
        '''
        '''
        lm = np.float32(lm)

        res = []
        for frame in lm:
            tform = tf.estimate_transform('affine', frame.T,self.mean_face.T)
            res.append(tform(frame.T).T)
        lm = np.array(res)

        return lm

    def _apply_pca(self,X,fit_para=0):
        '''
        '''
        if self.n_components==None:return X
        if fit_para:
            self.pca = PCA(n_components=self.n_components)
            self.pca.fit(X)
        return self.pca.transform(X)

    def _standardization(self,X,fit_para=0):
        '''
        '''
        if self.n_components==None:return X
        if fit_para:
            self.mean_2 = np.array([X.mean(0)]).T
        X = (X.T-self.mean_2).T
        if fit_para:
            self.std_2 = np.array([X.std(0)]).T
        X = (X.T/self.std_2).T

        return X

class landmarks(process_data):
    '''
    '''
    def __init__(self, n_components=None, rm_seq_mean=0, median_window=0, verbose=0):
        '''
        '''
        self.verbose=verbose
        self.n_components = n_components
        self.median_window = median_window
        self.mean_face = cPickle.load(gzip.open(pwd+'/mean_face_dlib.pklz','rb'))
        self.rm_seq_mean = rm_seq_mean

    def fit(self, lm):
        '''
        '''
        lm= deepcopy(lm)
        lm = [np.float32(xx) for xx in lm]

        if isinstance(lm, list):
            lm = np.concatenate(lm,0)

        for i in range(lm.shape[1]):
            lm[:,i]=imputer.fit_transform(lm[:,i])
    

        self.mean_face = np.mean(lm,0)
        self.mean_face = (self.mean_face.T-self.mean_face.mean(1)).T
        self.mean_face = (self.mean_face.T/np.max(np.abs(self.mean_face),1)).T

        # lm = self._affine_transform(lm,1)
        # X = np.hstack((lm[:,0,:],lm[:,1,:]))
        # X = self._apply_pca(X,1)
        # X = self._standardization(X,1)

        return self

    def affine(self,lm):
        lm = deepcopy(lm)
        lm = self._affine_transform(lm,0)
        return lm

    def transform(self, lm, subject_id = None):
        '''
        '''
        lm = deepcopy(lm)

        if isinstance(lm, list):
            out = [self._transform(xx) for xx in lm]
        else:
            out = self._transform(lm)

        out = [imputer.fit_transform(xx) for xx in out]

        if self.rm_seq_mean==1:out = [xx-np.median(xx,0) for xx in out]
        
        return out
    
    def _transform(self, lm):
        '''
        '''
        for i in range(lm.shape[1]):
            lm[:,i]=imputer.fit_transform(lm[:,i])

        lm = self._affine_transform(lm)
        X = np.hstack((lm[:,0,:],lm[:,1,:]))
        if self.median_window:
            for dim in range(X.shape[1]):
                X[:,dim]-=medfilt(X[:,dim],self.median_window)
        return X
