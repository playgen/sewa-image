from BASE import BASE 
from BASE import MultiOutputTransform
from sklearn.neural_network import MLPClassifier
import numpy as np

class MLP(BASE):

    def __init__(self,X,y, C=[1.,[40,2]], **kwargs):
        BASE.__init__(self, X, y, **kwargs)
        self.C = C

        alpha = np.power(10.,np.arange(-3,3))
        hidden_layer_sizes=[(5, 2),(40,2),(100)]
        self.C_default = []
        for a in alpha:
            for h in hidden_layer_sizes:
                self.C_default.append([a,h])


        self.type='classification'

    def fit(self, X, y, lock=None):
        """
        """

        X = np.vstack(X)
        y = np.vstack(y)
        y = MultiOutputTransform(self.enc, y)

        self.MO_clf = []

        for y_ in y.T:
            clf = MLPClassifier(
                alpha = self.C[0], 
                hidden_layer_sizes = self.C[1],
                max_iter = self.max_iter,
            )
            clf.fit(X, y_)
            self.MO_clf.append(clf)

        return self

