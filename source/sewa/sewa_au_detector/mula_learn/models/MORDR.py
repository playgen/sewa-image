from BASE import BASE 
from BASE import MultiOutputTransform
import mord
import numpy as np

class MORDR(BASE):

    def __init__(self,X,y, C=1., **kwargs):
        BASE.__init__(self, X, y, **kwargs)
        self.C=C
        self.C_default = np.power(10.,np.arange(-5,10))
        self.C_default = np.hstack((self.C_default,[0]))
        self.type='regression'

    def fit(self, X, y, lock=None):
        """
        """
        X = np.vstack(X)
        y = np.vstack(y)
        y = MultiOutputTransform(self.enc, y)

        self.MO_clf = []

        for y_ in y.T:
            clf = mord.OrdinalRidge(
                alpha=self.C,
            )
            clf.fit(X, y_)
            self.MO_clf.append(clf)

        return self
