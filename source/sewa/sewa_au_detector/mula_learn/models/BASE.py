import numpy as np
from sklearn import preprocessing

def sample_weight(y):
    U = np.unique(y)
    w = np.zeros(U.shape)
    for i, u in enumerate(U):
        w[i] = float(np.sum(y==u))
    w=w/float(w.sum())
    return w[y]


def MultiOutputLabelEncoder(y):
    enc = []
    for y_ in y.T:
        tmp = preprocessing.LabelEncoder()
        tmp.fit(y_)
        enc.append(tmp)
    return enc


def MultiOutputTransform(enc, y):
    for i in range(len(enc)):
        y[:, i] = enc[i].transform(y[:, i])
    return y


class BASE():
    def __init__(self, X, y, verbose=0, max_iter=5000):
        """
        """
        X = np.vstack(X)
        y = np.vstack(y)
        print X.shape
        self.verbose = verbose
        self.max_iter = max_iter

        self.N = len(np.unique(y))
        self.enc = MultiOutputLabelEncoder(y)

    def predict(self,X):
        return [self._predict(xx) for xx in X]

    def _predict(self, X):
        """
        """
        res = []
        for clf, enc in zip(self.MO_clf, self.enc):
            tmp = clf.predict(X)
            if self.type=='classification' and np.all(np.unique(tmp)!=(False,True)):
                tmp = enc.inverse_transform(tmp)
            res.append(tmp)

        y_hat = np.vstack(res).T
        return y_hat
