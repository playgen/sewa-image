import numpy as np
import theano.tensor as TT
import theano as T
import TeaSpoon
import utils
from GM import GM



class MOR(GM):

    def _init_para(self, X, y, edges):
        '''
        '''
        np.random.seed(1)
        F, C, M = X.shape[1], len(np.unique(y)), y.shape[1]

        if len(edges)==0:edges=[[0,0]]

        w = np.random.uniform(-1,1,(M, F))
        d = np.ones((M, C-2))

        b = np.ones(M) * (1 - 0.5 * C)
        s = np.ones(M)
        e = np.zeros(( len(edges), C,C))

        para = {}
        para['b'] = TeaSpoon.parameter(b)
        para['w'] = TeaSpoon.parameter(w)
        para['d'] = TeaSpoon.parameter(d)
        para['s'] = TeaSpoon.parameter(s)

        para['e'] = TeaSpoon.parameter(e)
        para['edges'] = TeaSpoon.parameter(edges, const=True)

        return para, [F,C,M]

    def _compute_node_potentials(self, para, X):
        '''
        '''
        w = para['w'].value
        b = para['b'].value
        d = para['d'].value
        s = para['s'].value
        b = b.dimshuffle(0, 'x')

        NU = TT.extra_ops.cumsum(
            TT.concatenate((b, TT.sqr(d)), axis=1),
            axis=1)

        NU = TT.concatenate(
            (-1e20 * TT.ones_like(b), NU, 1e20 * TT.ones_like(b)),
            axis=1)

        NU = NU.dimshuffle('x', 0, 1)
        Z = T.dot(w, X.T).dimshuffle(1, 0, 'x')
        Z = TT.extra_ops.repeat(Z, NU.shape[2], 2)
        S = s.dimshuffle('x', 0, 'x')

        cdf = utils.margins.sigmoid(NU, TT.sqr(S), Z)
        n  = cdf[:, :, :-1]-cdf[:, :, 1:]
        np = utils.comp_node_potentials( n, self.y_star )
        return np 

    def _compute_edge_potentials(self, para, X):
        '''
        '''
        edges = para['edges'].value.astype('int8')
        e = para['e'].value

        e = TT.exp(TT.tile(e,(X.shape[0],1,1,1)))
        e = e/e.sum(3).sum(2).dimshuffle(0,1,'x','x')

        ep = utils.comp_edge_potentials( e, self.y_star, edges )

        return ep 

    def _regularization_cost(self,para,X):
        cost = T.shared(0)
        cost += 1./self.C[0] * TT.sum(TT.sqr(para['w'].value))
        cost += 1./self.C[1] * TT.sum(TT.sqr(TT.log(TT.sqr(para['s'].value))))
        return cost

if __name__ == "__main__":

    X = np.random.rand(100,40)-0.5
    y = np.random.randint(0,5,[100,4])
    y_rand = np.random.randint(0,5,[100,4])

    clf = MOR(X,y, verbose=2)
    TeaSpoon.debug(clf._compute_edge_potentials,[clf.p1,X])
    TeaSpoon.debug(clf._compute_node_potentials,[clf.p1,X])
    clf.fit(X, y, X, y_rand )
