import numpy as np
import theano as T
import theano.tensor as TT
import TeaSpoon
import utils
import sys,os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'utils')))
import metrics


class GM():

    def __init__(self, X, y, C=[1e-20,1e-20], max_iter=100, verbose=0, graph='full'):
        """
        """
        self.C = C
        self.max_iter = max_iter
        self.verbose = verbose

        if graph=='full':
            edges = utils.graph_algorithms.fully_connected_graph(y)

        if graph=='chow_liu':
            edges = utils.graph_algorithms.chow_liu(y)

        if graph=='independent':
            edges = []

        if self.verbose>1:
            print 'edges:'
            print edges


        self.p0, self.shape = self._init_para(X, y, edges)
        self.p1 = self.p0

        # dry-run
        if self.verbose>1:print 'quick check ...'
        y_star, idx = utils.comp_config(self.shape, y)
        self.y_star = T.shared(y_star)
        TeaSpoon.debug(self._loss, [self.p0, X, idx])
        TeaSpoon.debug(self._predict, [self.p0, X])

    def _loss(self, para, X, lab):
        '''
        '''
        np = self._compute_node_potentials(para,X)
        ep = self._compute_edge_potentials(para,X)
        scores = TT.sum(ep,axis=2)+TT.sum(np,axis=2)

        Z = TT.sum(TT.exp(scores),axis=1)

        idx = TT.arange(X.shape[0]).astype('int64')
        lab = lab.astype('int64')

        S = TT.sum(np[idx,lab],1) + TT.sum(ep[idx,lab],1)

        cll = S - TT.log(Z) 

        loss = TT.mean(-cll) + self._regularization_cost(para,X)

        return loss

    def _predict(self, para, X):
        """
        """
        np = self._compute_node_potentials(para,X)
        ep = self._compute_edge_potentials(para,X)
        scores = TT.sum(ep,axis=2)+TT.sum(np,axis=2)
        idx    = TT.argmax(scores,axis=1)
        return self.y_star[idx]

    def fit(self, X, y, X_te=None,y_te=None,lock=None):
        """
        """
        if lock!=None:lock.acquire()
        if self.verbose>1:print 'compile loss function ...'
        idx = np.ones_like(y[:,0])
        self.loss_C, self.loss_grad_C = TeaSpoon.compile(
            self._loss, [self.p0, X, idx], jac=True)
     
        if self.verbose>1:print 'compile prediction function ...'
        self.predict_C = TeaSpoon.compile(
            self._predict, [self.p0, X], jac=False)
        if lock!=None:lock.release()
        

        if self.verbose == 0:callback = None
        if self.verbose >= 1:
            def callback(pi):
                '''
                '''
                out = {}
                out['Loss'] =  TeaSpoon.exe(self.loss_C, [pi, X, idx])

                y_hat = self.predict(X, pi)
                out['ICC(tr)'] = metrics.ICC(y_hat,y).mean()

                if X_te!=None:
                    y_hat = self.predict(X_te, pi)
                    out['ICC(te)'] = metrics.ICC(y_hat,y_te).mean()

                opt = {'freq':5}
                return out, opt

        y_star, idx = utils.comp_config(self.shape, y)

        self.p1, self.cost = TeaSpoon.optimize(
            fun=self.loss_C,
            p0=self.p0,
            jac=self.loss_grad_C,
            callback=callback,
            method='CG',
            args=(X, idx),
            options = {'maxiter': self.max_iter, 'disp': self.verbose > 1}
        )
        return self

    def predict(self, X, pi='self'):
        """
        """
        if pi=='self':pi=self.p1
        y_hat = TeaSpoon.exe(self.predict_C, [pi, X])
        return y_hat
