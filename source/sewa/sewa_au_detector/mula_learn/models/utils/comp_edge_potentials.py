import numpy as np
import itertools
from theano import shared as TS
import theano.tensor as TT
from comp_config import comp_config
np.random.seed(1)

def numpy_comp_edge_potentials(edge_pot,y_all,edges):
    '''
    IN:
        edge_pot: [F x E x I x I] 
        y_all:    [T x E] 
            F : number of frames
            E : number of edges 
            T : number of possible ouputs
            I : number of states
    OUT:
        S:       [F x T x E]
        node potentials for each cobination of labels (T)
    '''

    e0 = edges[:,0]
    id0 = np.tile(np.arange(y_all.shape[0]),(len(e0),1)).T
    id1 = np.tile(e0,(y_all.shape[0],1))
    id0 = id0.reshape(-1)
    id1 = id1.reshape(-1)
    y0 = y_all[id0,id1].reshape(y_all.shape[0],len(e0))

    e1 = edges[:,1]
    id0 = np.tile(np.arange(y_all.shape[0]),(len(e1),1)).T
    id1 = np.tile(e1,(y_all.shape[0],1))
    id0 = id0.reshape(-1)
    id1 = id1.reshape(-1)
    y1 = y_all[id0,id1].reshape(y_all.shape[0],len(e1))

    id0 = np.tile(np.arange(edge_pot.shape[0]),(len(edges),y_all.shape[0],1)).T
    id1 = np.tile(np.arange(len(edges)),(edge_pot.shape[0],y_all.shape[0],1))
    id2 = np.tile(y0,(edge_pot.shape[0],1,1))
    id3 = np.tile(y1,(edge_pot.shape[0],1,1))

    id0 = id0.reshape(-1)
    id1 = id1.reshape(-1)
    id2 = id2.reshape(-1)
    id3 = id3.reshape(-1)

    out = edge_pot[id0,id1,id2,id3].reshape(edge_pot.shape[0],y_all.shape[0],len(edges))
    return out
    
def numpy_comp_edge_potentials_loopy(edge_pot,y_all,edges):
    '''
    IN:
        edge_pot: [F x N x I x I] 
        y_all:    [T x N] 
            F : number of frames
            N : number of edges 
            T : number of possible ouputs
            I : number of states
    OUT:
        S:       [F x T x N]
        node potentials for each cobination of labels (T)
    '''
    res = np.zeros((edge_pot.shape[0],y_all.shape[0],len(edges)))
    idx = np.arange(len(edges))
    for f in range(res.shape[0]):
            for y_ in range(res.shape[1]):
                e0 = y_all[y_][edges[:,0]]
                e1 = y_all[y_][edges[:,1]]
                res[f,y_] = edge_pot[f,idx,e0,e1]
    return res

def comp_edge_potentials(edge_pot,y_all,edges):
    '''
    IN:
        edge_pot: [F x E x I x I] 
        y_all:    [T x E] 
            F : number of frames
            E : number of edges 
            T : number of possible ouputs
            I : number of states
    OUT:
        S:       [F x T x E]
        node potentials for each cobination of labels (T)
    '''
    y_all = y_all.astype('int8')

    e0 = edges[:,0]
    id0 = TT.tile(TT.arange(y_all.shape[0]),(e0.shape[0],1)).T
    id1 = TT.tile(e0,(y_all.shape[0],1))
    id0 = id0.flatten()
    id1 = id1.flatten()
    y0 = y_all[id0,id1].reshape((y_all.shape[0],e0.shape[0]))

    e1 = edges[:,1]
    id0 = TT.tile(TT.arange(y_all.shape[0]),(e1.shape[0],1)).T
    id1 = TT.tile(e1,(y_all.shape[0],1))
    id0 = id0.flatten()
    id1 = id1.flatten()
    y1 = y_all[id0,id1].reshape((y_all.shape[0],e1.shape[0]))

    id0 = TT.tile(TT.arange(edge_pot.shape[0]),(edges.shape[0],y_all.shape[0],1)).T
    id1 = TT.tile(TT.arange(edges.shape[0]),(edge_pot.shape[0],y_all.shape[0],1))
    id2 = TT.tile(y0,(edge_pot.shape[0],1,1))
    id3 = TT.tile(y1,(edge_pot.shape[0],1,1))

    id0 = id0.flatten()
    id1 = id1.flatten()
    id2 = id2.flatten()
    id3 = id3.flatten()

    out = edge_pot[id0,id1,id2,id3].reshape((edge_pot.shape[0],y_all.shape[0],edges.shape[0]))
    return out

if __name__ == '__main__':

    # 20 frames
    # 6 nodes
    # 2 states

    node_p = np.random.rand(200,6,2) 
    y = np.random.randint(0,2,(200,6))
    y_star, i = comp_config(y)

    edges = []
    C,M = len(np.unique(y)), y.shape[1]
    for e in itertools.combinations(range(M),2):edges.append(e)
    edges = np.array(edges)
    edge_p = np.random.rand(20,len(edges),C,C)

    SSc = numpy_comp_edge_potentials_loopy(edge_p,y_star,edges)

    #frame 12, config 42, edge 5
    y_star[i[12]]
    i0,i1 = y[12][edges[5]]
    assert edge_p[12,5,i0,i1] == SSc[12,i[12],5]

    SS = numpy_comp_edge_potentials(edge_p,y_star,edges)
    assert np.all((SS == SSc))

    Theano_SS = comp_edge_potentials(TS(edge_p),TS(y_star),TS(edges))
    assert np.all(SS==Theano_SS.eval())
