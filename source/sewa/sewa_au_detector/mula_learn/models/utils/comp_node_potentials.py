import numpy as np
import itertools
from theano import shared as TS
import theano.tensor as TT
from comp_config import comp_config
np.random.seed(1)

def numpy_comp_node_potentials(node_pot,y_all):
    '''
    IN:
        node_pot: [F x N x I] 
        y_all:    [T x N] 
            F : number of frames
            N : number of nodes
            T : number of possible ouputs
            I : number of states
    OUT:
        S:       [F x T x N]
        node potentials for each cobination of labels (T)
    '''
    Y_all = np.tile(y_all,(node_pot.shape[0],1,1))
    id0 = np.tile(np.arange(Y_all.shape[0]),(Y_all.shape[2],Y_all.shape[1],1)).transpose(2,1,0)
    id1 = np.tile(np.arange(Y_all.shape[2]),(Y_all.shape[0],Y_all.shape[1],1))
    id0 = id0.reshape(-1)
    id1 = id1.reshape(-1)
    id2 = Y_all.reshape(-1)
    S = node_pot[id0,id1,id2].reshape(Y_all.shape)
    return S

def numpy_comp_node_potentials_loopy(node_pot,y_all):
    '''
    IN:
        node_pot: [F x N x I] 
        y_all:    [T x N] 
            F : number of frames
            N : number of nodes
            T : number of possible ouputs
            I : number of states
    OUT:
        S:       [F x T x N]
        node potentials for each cobination of labels (T)
    '''
    res = np.zeros((node_pot.shape[0],y_all.shape[0],y_all.shape[1]))
    idx = np.arange(y_all.shape[1])
    for f in range(res.shape[0]):
            for y_ in range(res.shape[1]):
                I = y_all[y_]
                res[f,y_] = node_pot[f,idx,I]
    return res

def comp_node_potentials(node_pot,y_all):
    '''
    IN:
        node_pot: [F x N x I] 
        y_all:    [T x N] 
            F : number of frames
            N : number of nodes
            T : number of possible ouputs
            I : number of states
    OUT:
        S:       [F x T x N]
        node potentials for each cobination of labels (T)
    '''
    Y_all = TT.tile(y_all,(node_pot.shape[0],1,1)).astype('int8')
    id0 = TT.tile(TT.arange(Y_all.shape[0]),(Y_all.shape[2],Y_all.shape[1],1)).transpose(2,1,0)
    id1 = TT.tile(TT.arange(Y_all.shape[2]),(Y_all.shape[0],Y_all.shape[1],1))
    id0 = id0.flatten()
    id1 = id1.flatten()
    id2 = Y_all.flatten()
    S = node_pot[id0,id1,id2].reshape(Y_all.shape)
    return S

def comp_node_potentials_loopy(node_pot,y_all):
    '''
    IN:
        node_pot: [F x N x I] 
        y_all:    [T x N] 
            F : number of frames
            N : number of nodes
            T : number of possible ouputs
            I : number of states
    OUT:
        S:       [F x T x N]
        node potentials for each cobination of labels (T)
    '''
    res = np.zeros((node_pot.shape[0],y_all.shape[0],y_all.shape[1]))
    idx = np.arange(y_all.shape[1])
    for f in range(res.shape[0]):
            for y_ in range(res.shape[1]):
                I = y_all[y_]
                res[f,y_] = node_pot[f,idx,I]
    return res

if __name__ == '__main__':

    # 20 frames
    # 6 nodes
    # 2 states

    # test numpy version

    node_p = np.random.rand(20,6,2) 
    y = np.random.randint(0,2,(20,6))
    y_star, i = comp_config(y)

    SS = numpy_comp_node_potentials(node_p,y_star)
    SSc = numpy_comp_node_potentials_loopy(node_p,y_star)
    assert np.all(SS==SSc)

    # test theano tesnor version

    Theano_SS = comp_node_potentials(TS(node_p),TS(y_star))
    assert np.all(SS==Theano_SS.eval())
