import numpy as np
import theano as T
import theano.tensor as TT
import TeaSpoon
import utils
import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'utils')))
import metrics
from SOR import SOR


class ML_SOR(SOR):

    def __init__(self, X, y, L=4, **kwargs):
        """
        """
        SOR.__init__(self, X, y, **kwargs)

        F,C,M = self.shape

        w0 = np.random.uniform(-1,1,(L, F))
        b0 = np.zeros(L)
        self.p0['w0'] = TeaSpoon.parameter(w0)
        self.p0['b0'] = TeaSpoon.parameter(b0)

        w = np.random.uniform(-1,1,(M, L))
        self.p0['w'] = TeaSpoon.parameter(w)


        # # dry-run
        TeaSpoon.debug(self._pdf,  [self.p0, X])
        TeaSpoon.debug(self._loss, [self.p0, X, y])
        TeaSpoon.debug(self._predict, [self.p0, X])

    def _pdf(self, para, X):
        cdf = self._cdf(para, X)
        pdf = cdf[:, :, :-1]-cdf[:, :, 1:]
        return pdf

    def _loss(self, para, X, y):
        '''
        '''
        w0 = para['w0'].value
        b0 = para['b0'].value
        X = TT.nnet.sigmoid( T.dot(X,w0.T)+b0.dimshuffle('x',0) )

        P = self._pdf(para, X)

        if self.loss_function=='mse':
            E = utils.statistics.expectation(P)
            loss = TT.mean( TT.sqr(E-y) )

        if self.loss_function=='ncll':
            CLL = utils.statistics.compute_cll(P, y)
            loss = -TT.mean( CLL )

        loss += 1./self.C[0] * TT.sum(TT.sqr(para['w'].value))
        loss += 1./self.C[1] * TT.sum(TT.sqr(para['w0'].value))
        return loss

    def _predict(self, para, X):
        w0 = para['w0'].value
        b0 = para['b0'].value
        X = TT.nnet.sigmoid( T.dot(X,w0.T)+b0.dimshuffle('x',0) )

        P = self._pdf(para, X)

        if self.ouput=='expectation':
            return utils.statistics.expectation(P)
        if self.ouput=='MAP':
            return TT.argmax(P, 2)
        if self.ouput=='probability':
            return P
