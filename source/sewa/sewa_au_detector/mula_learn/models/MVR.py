import numpy as np
import theano.tensor as TT
import theano as T
import TeaSpoon
import utils
from GM import GM



class MVR(GM):

    def _init_para(self, X, y, edges):
        '''
        '''
        np.random.seed(1)
        F, C, M = X.shape[1], len(np.unique(y)), y.shape[1]

        if len(edges)==0:edges=[[0,0]]

        w = np.random.rand(C, M, F) - 0.5
        b = np.zeros((M,C))
        e = np.zeros(( len(edges), C,C))


        para = {}
        para['b'] = TeaSpoon.parameter(b)
        para['w'] = TeaSpoon.parameter(w)
        para['e'] = TeaSpoon.parameter(e)
        para['edges'] = TeaSpoon.parameter(edges, const=True)

        return para, [F,C,M]

    def _compute_node_potentials(self, para, X):
        '''
        '''
        w = para['w'].value
        b = para['b'].value

        n = TT.tensordot(X,w.T,axes=1)+b.dimshuffle('x',0,1)
        np = utils.comp_node_potentials( n, self.y_star )
        return np 

    def _compute_edge_potentials(self, para, X):
        '''
        '''
        edges = para['edges'].value.astype('int8')
        e = para['e'].value

        e = TT.tile(e,(X.shape[0],1,1,1))
        ep = utils.comp_edge_potentials( e, self.y_star, edges )

        return ep 

    def _regularization_cost(self,para,X):
        cost = T.shared(0)
        if self.C!=None:
            cost += 1./self.C[0] * TT.sum(TT.sqr(para['w'].value))
            cost += 1./self.C[1] * TT.sum(TT.sqr(para['e'].value))
        return cost

if __name__ == "__main__":

    X = np.random.rand(100,40)-0.5
    y = np.random.randint(0,5,[100,4])
    y_rand = np.random.randint(0,5,[100,4])

    clf = MVR(X,y, verbose=2)
    clf.fit(X, y, X, y_rand )
