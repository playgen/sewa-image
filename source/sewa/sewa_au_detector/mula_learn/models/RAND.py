import numpy as np
class RAND():
    def __init__(self,X,y):
        y = np.vstack(y)
        self.N = np.unique(y)
        self.M = y.shape[1]
        self.C_default=[0]

    def fit(self,X,y,lock=None):pass
    def predict(self,X):
        y_hat = []
        for xx in X:
            l = xx.shape[0]
            y_hat.append(np.random.choice(self.N,[l,self.M]))
        return y_hat
