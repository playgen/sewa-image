import cPickle
import gzip
import os
module_path = os.path.dirname(__file__)

def load_disfa():
    data = cPickle.load(gzip.open(module_path+'/disfa_subset.pklz','rb'))
    info = {}
    info['y_name'] = [1,2,4,5,6,9,12,15,17,20,25,26]
    info['name'] = 'disfa'
    return data['X'],data['y'],data['S'], info

