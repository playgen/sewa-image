import numpy as np
import matplotlib.pyplot as plt
from sklearn import covariance

import matplotlib.cm as cm
def _plot_matrix(data,xname = None,xlabel= '',path = '/tmp/test.pdf'):
    fig, ax = plt.subplots()
    plt.imshow(data,origin='lower',interpolation='nearest',cmap=cm.Spectral_r,aspect='auto')
    N = data.shape[1]
    if xname == None:
        xname = range(N)

    ax.set_xticklabels( xname )
    ax.set_xticks( range(N) )
    ax.set_xlabel(xlabel)

    ax.set_yticklabels( xname )
    ax.set_yticks( range(N) )
    ax.set_ylabel(xlabel)

    for x in range(data.shape[0]):
        for y in range(data.shape[1]):
            plt.text(x-0.35, y-0.1, np.int16(data[x,y]*100)/100.,size=14)

    plt.tight_layout()
    plt.savefig(path)

def label_distribution(data,
               m_name='',
               s_name='',
               title='',
               ylabel = '',
               xlabel = '',
               xname = [],
               ylim = [],
               legend_size = 22,
               legend_loc = 4,
               path='/tmp/test.pdf'
               ):

    fig, ax = plt.subplots()
    classes = np.unique(data)

    c_map = cm.bwr(np.linspace(0, 1, len(classes)))
    for i in classes[::-1]:
        tmp = np.sum(data<=i,1)
        plt.bar(range(len(tmp)),tmp,color=c_map[i])


    ax.set_xticklabels( xname ,fontsize=24)
    ax.set_xticks( np.arange(len(xname))+0.4 )
    # ax.set_xlabel(xlabel,fontsize=20)
    ax.set_ylabel(ylabel,fontsize=26)
    ax.set_ylim((0,data.shape[1]*1.05))
    ax.ticklabel_format(axis='y', style='sci', scilimits=(-2,2))
    ax.tick_params(axis='y', labelsize=20)
    ax.legend(classes[::-1],fontsize=legend_size,loc = legend_loc)
    plt.savefig(path)


def corr(y, axis_name, path):
    C = np.cov(y.T)

    print axis_name
    print np.int16(C*100)/100.

    _plot_matrix(
        data = C,
        xname=axis_name,
        xlabel='Action Unit',
        path=path
    )

def lasso(y,axis_name,path,alpha=0.01):
    C = np.cov(y.T)
    P = np.linalg.inv(C)
    G = covariance.GraphLasso(alpha=alpha)
    G.fit(y)
    G = G.get_precision()

    print axis_name
    print np.int16(G*100)/100.

    _plot_matrix(
        data = G,
        xname=axis_name,
        xlabel='Action Unit',
        path=path
    )
def prec(y,axis_name,path):
    C = np.cov(y.T)
    P = np.linalg.inv(C)

    print axis_name
    print np.int16(P*100)/100.

    _plot_matrix(
        data = P,
        xname=axis_name,
        xlabel='Action Unit',
        path=path
    )

def hist(y,axis_name,path):
    label_distribution(
        y.T,
        path=path,
        xname=axis_name,
        xlabel='Action Unit',
        ylabel='# - active',
    )
    pass
