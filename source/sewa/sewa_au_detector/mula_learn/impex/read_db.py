import h5py
import numpy as np
import glob
from collections import defaultdict

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cv2

def plot_seq(frames, prediction=None, ground_truth=None, N=7, ylim=[0,5]):
    gs = gridspec.GridSpec(prediction.shape[1]+1, N)
    gs.update(left=0.05, right=0.95, wspace=0.00)
    key_frames = np.uint32(np.arange(N)*(len(frames)/float(N)))
    for i,idx in enumerate(key_frames):
        ax = plt.subplot(gs[0, i])
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        im = cv2.cvtColor(cv2.imread(frames[idx]),cv2.COLOR_BGR2RGB)
        im = im[140:-140,100:-100]
        ax.imshow(im)
    AU = ['AU1','AU2','AU4','AU12','AU17']
    for i in range(prediction.shape[1]):
        ax = plt.subplot(gs[1+i, :])
        ax.plot(prediction[:,i],'b-',linewidth=2)
        ax.text(0.95, 0.01,AU[i],fontsize=30)
        ax.fill_between(np.arange(ground_truth.shape[0]), 0, ground_truth[:,i],facecolor='green', alpha=0.4)
        ax.set_ylim(*ylim)
        if i==prediction.shape[1]-1:
            ax.set_xlabel('Frame',fontsize=20)
        else:
            ax.get_xaxis().set_visible(False)

    return ax
