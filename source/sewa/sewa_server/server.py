import cherrypy
from webcontrollers.rootwebcontroller import RootWebController
from webcontrollers.analysiswebcontroller import AnalysisWebController


def mount_web_controllers():
    cherrypy.tree.mount(RootWebController())
    cherrypy.tree.mount(AnalysisWebController(), "/analysis",
                        {"/": {"request.dispatch": cherrypy.dispatch.MethodDispatcher(),
                               "tools.sessions.on": True
                               }})


if __name__ == "__main__":
    mount_web_controllers()
    cherrypy.config.update("server.conf")
    cherrypy.engine.start()
