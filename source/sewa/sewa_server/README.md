# SEWA Server Utility

This is the web server utlity for remote testing of the processing pipelines.

# Installation Steps:

1) Install Miniconda by following the steps on: https://conda.io/miniconda.html
2) Create the 'sewa' environment: conda create --name sewa python=2.7
3) Activate the 'sewa' environment: source activate sewa (Ubuntu) or activate sewa (Windows)
4) Install cherrypy: conda install cherrypy
5) Install moviepy: pip install ffmpy

# How to Use / Test

To start the server, please run (when 'sewa' is activated): python server.py. You can then visit the server at port 8080 using a web browser.
