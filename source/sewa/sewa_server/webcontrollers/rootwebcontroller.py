import cherrypy


class RootWebController(object):
    @cherrypy.expose
    def index(self):
        return "SEWA Server"
