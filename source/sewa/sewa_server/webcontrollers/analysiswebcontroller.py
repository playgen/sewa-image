import cherrypy
import collections
from core.analyzer import Analyzer


# noinspection PyPep8Naming
@cherrypy.expose()
class AnalysisWebController(object):
    @cherrypy.tools.json_out()
    def POST(self, files):
        if not isinstance(files, list):
            files = [files]

        results, errors = Analyzer.analyze_from_files_streams(files)
        return results, errors
