import imp
import csv


def analyze(audio_file_path, output_dir):
    values = {}
    errors = []

    try:
        # LLDs
        lld_extractor = imp.load_source("sewa_audio_lld_extractor", "../sewa_audio_lld_extractor"
                                                                    "/sewa_audio_lld_extractor.py")
        lld_file_path = output_dir + "/LLD.csv"
        lld_extractor.process(audio_file_path, lld_file_path)

        # Classifiers
        open_xbow = imp.load_source("sewa_openxbow",
                                        "../sewa_openxbow/sewa_openxbow.py")
        classifier_file_path = output_dir + "/BAOW.libsvm"
        open_xbow.process(lld_file_path, classifier_file_path)

        # Value Prediction
        value_predictor = imp.load_source("sewa_val_predictor",
                                    "../sewa_val_predictor/sewa_val_predictor.py")
        values_file_path = output_dir + "/VAL.csv"
        value_predictor.process(classifier_file_path, values_file_path)

        # Transform Data
        with open(values_file_path, "rb") as csvfile:
            reader = csv.reader(csvfile)
            data = list(reader)

        for column_index in range(len(data[0])):
            values[data[0][column_index]] = data[1][column_index]

    except Exception as exception:
        errors.append(str(exception))

    return values, errors