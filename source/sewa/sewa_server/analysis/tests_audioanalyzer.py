import unittest
import testconfig
from analysis import audioanalyzer
from core import analyzer


class AudioAnalyzerTests(unittest.TestCase):
    def test_analyze_wav(self):
        analyzing_dir = analyzer.setup_analyzing_dir()
        results, errors = audioanalyzer.analyze(testconfig.TEST_WAV, analyzing_dir)

        analyzer.cleanup_analyzing_dir(analyzing_dir)

        self.assertEqual(0, len(errors))
        self.assertItemsEqual(testconfig.EXPECTED_AUDIO_ANALYSIS_KEYS, results.keys())


if __name__ == '__main__':
    unittest.main()
