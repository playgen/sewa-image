import json
import os
import unittest

import cherrypy
import requests

import server
import testconfig


# noinspection PyPep8Naming
def setUpModule():
    server.mount_web_controllers()
    cherrypy.config.update({"server.socket_host": testconfig.HOST,
                            "server.socket_port": testconfig.PORT})
    cherrypy.engine.start()


# noinspection PyPep8Naming
def tearDownModule():
    cherrypy.engine.exit()


def compose_uri(suffix):
    return "http://" + testconfig.HOST + ":" + str(testconfig.PORT) + suffix


class ServerTests(unittest.TestCase):
    def test_index(self):
        response = requests.get(compose_uri("/"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, "SEWA Server")

    def test_analysis_webm(self):
        # Arrange
        webm_object = open(testconfig.TEST_WEBM, "rb")
        files = [("files", (os.path.basename(webm_object.name), webm_object))]
        self.analysis(files)

    def test_analysis_wav_avi(self):
        # Arrange
        avi_object = open(testconfig.TEST_AVI, "rb")
        wav_object = open(testconfig.TEST_WAV, "rb")
        files = [("files", (os.path.basename(avi_object.name), avi_object)),
                 ("files", (os.path.basename(wav_object.name), wav_object))]
        self.analysis(files)

    def analysis(self, files):
        # Act
        response = requests.post(compose_uri("/analysis"), files=files)

        # Assert
        self.assertEqual(response.status_code, 200)
        analysis_results, errors = json.loads(response.content)
        analysis_result_keys = analysis_results.keys()
        self.assertEqual(0, len(errors))
        self.assertItemsEqual(testconfig.EXPECTED_ANALYSIS_KEYS, analysis_result_keys)


if __name__ == '__main__':
    unittest.main()
