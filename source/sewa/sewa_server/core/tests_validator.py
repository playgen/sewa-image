import os
import unittest
from core import validatorconfig
from core.validator import Validator


class ValidatorTests(unittest.TestCase):
    def test_is_valid(self):
        files_to_validate = ("nonexistantdir/nonexistantfile.avi", "nonexistantdir/nonexistantfile.wav")
        video_file_path, audio_file_path, errors = Validator.filter_valid(files_to_validate)

        self.assertIsNotNone(video_file_path)
        self.assertIsNotNone(audio_file_path)
        self.assertEqual(0, len(errors))
        self.assertEqual(validatorconfig.VIDEO_FILE_EXTENSION, os.path.splitext(video_file_path)[1])
        self.assertEqual(validatorconfig.AUDIO_FILE_EXTENSION, os.path.splitext(audio_file_path)[1])

    def test_is_invalid(self):
        files_to_validate = ("nonexistantdir/nonexistantfile.invalidtype1", "nonexistantdir/nonexistantfile.invalidtype2")
        video_file_path, audio_file_path, errors = Validator.filter_valid(files_to_validate)

        self.assertIsNone(video_file_path)
        self.assertIsNone(audio_file_path)
        self.assertNotEqual(0, len(errors))


if __name__ == '__main__':
    unittest.main()