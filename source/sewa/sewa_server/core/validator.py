import os
import validatorconfig


class Validator(object):
    @staticmethod
    def filter_valid(names_to_validate):
        video_name = None
        audio_name = None
        errors = []

        for name_to_validate in names_to_validate:
            name, ext = os.path.splitext(name_to_validate)
            if ext == validatorconfig.VIDEO_FILE_EXTENSION:
                if video_name is None:
                    video_name = name_to_validate
            elif ext == validatorconfig.AUDIO_FILE_EXTENSION:
                if audio_name is None:
                    audio_name = name_to_validate

            if audio_name is not None and video_name is not None:
                break

        if audio_name is None:
            errors.append("Didn't find file with extension: " + validatorconfig.AUDIO_FILE_EXTENSION)

        if video_name is None:
            errors.append("Didn't find file with extension: " + validatorconfig.VIDEO_FILE_EXTENSION)

        return video_name, audio_name, errors
