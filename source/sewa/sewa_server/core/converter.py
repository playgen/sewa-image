import ffmpy
import os
import converterconfig


class Converter(object):
    @staticmethod
    def convert(uncovnerted_file_paths):
        covnerted_file_paths = []

        for unconverted_file_path in uncovnerted_file_paths:
            name, ext = os.path.splitext(unconverted_file_path)
            ext = ext.lower()

            if ext in converterconfig.CONVERSION_MAPPINGS.keys():
                conversion_config = converterconfig.CONVERSION_MAPPINGS[ext]

                outputs = {}
                for output_config in conversion_config["outputs"]:
                    outputs[unconverted_file_path + output_config[0]] = output_config[1]

                ffmpeg = ffmpy.FFmpeg(
                    inputs={unconverted_file_path: conversion_config.get("input")},
                    outputs=outputs)

                ffmpeg.run()

                for converted_file_path in outputs.keys():
                    covnerted_file_paths.append(converted_file_path)

        return covnerted_file_paths
