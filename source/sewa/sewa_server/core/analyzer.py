import shutil
import uuid
import os
from converter import Converter
from validator import Validator
import analyzerconfig
from utils import logger
from analysis import audioanalyzer


def setup_analyzing_dir():
    processing_dir = analyzerconfig.ANALYZING_DIR + "/" + str(uuid.uuid4())
    os.makedirs(processing_dir)
    return processing_dir


def cleanup_analyzing_dir(analyzing_dir):
    shutil.rmtree(analyzing_dir)


def cleanup_files(files_paths):
    for file_path in files_paths:
        if os.path.isfile(file_path):
            os.remove(file_path)


def write_files(files, base_dir):
    written_files = []
    os.makedirs(base_dir)

    for unprocessed_file in files:
        write_path = base_dir + "/" + unprocessed_file.filename
        with open(write_path, "wb") as writer:
            writer.write(unprocessed_file.file.read())

        written_files.append(write_path)

    return written_files


class Analyzer(object):
    @staticmethod
    def analyze_from_files_streams(files):
        analyzing_dir = setup_analyzing_dir()
        files_paths = write_files(files, analyzing_dir + analyzerconfig.UNCONVERTED_DIR)
        results, errors = Analyzer.__analyze_from_files_paths(analyzing_dir, files_paths)
        return results, errors

    @staticmethod
    def analyze_from_files_paths(files_paths):
        analyzing_dir = setup_analyzing_dir()
        results, errors = Analyzer.__analyze_from_files_paths(analyzing_dir, files_paths)
        return results, errors

    @staticmethod
    def __analyze_from_files_paths(analyzing_dir, files_paths):
        logger.info("Starting Analysis on: " + ",".join(files_paths))
        logger.debug("Using folder: " + analyzing_dir)

        results = {}

        converted_files_paths = Converter.convert(files_paths)
        logger.debug("Converted to: " + ",".join(converted_files_paths))

        all_files_paths = tuple(converted_files_paths) + tuple(files_paths)
        video_file_path, audio_file_path, errors = Validator.filter_valid(all_files_paths)

        if len(errors) == 0:
            results, errors = Analyzer.__run_analysis(video_file_path, audio_file_path, analyzing_dir)

        cleanup_analyzing_dir(analyzing_dir)
        cleanup_files(converted_files_paths)

        if len(errors) > 0:
            logger.error("\n".join(errors))

        return results, errors

    @staticmethod
    def __run_analysis(video_file_path, audio_file_path, output_dir):
        audio_results, audio_errors = audioanalyzer.analyze(audio_file_path, output_dir)

        return audio_results, audio_errors
