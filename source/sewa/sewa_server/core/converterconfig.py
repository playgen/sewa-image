CONVERSION_MAPPINGS = {
    ".webm": {
        "input": ("-acodec", "libopus", "-vcodec", "libvpx"),
        "outputs": ((".avi", ["-vcodec", "libx264", "-r", "30", "-an"]),
                    (".wav", ["-r", "30"]))
    }
}
