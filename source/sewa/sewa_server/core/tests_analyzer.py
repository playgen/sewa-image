import os
import unittest

import testconfig
from core import analyzer
from core.analyzer import Analyzer


class AnalyzerTests(unittest.TestCase):
    def test_analyze_webm(self):
        files_paths = [testconfig.TEST_WEBM]
        self.analyze_success(files_paths)

    def test_analyze_avi_and_wav(self):
        files_paths = (testconfig.TEST_WAV, testconfig.TEST_AVI)
        self.analyze_success(files_paths)

    def test_convert_invalid(self):
        files_paths = ["nonexistantdir/nonexistantfile.nonexistantextension"]
        analyzing_dir = analyzer.setup_analyzing_dir()

        self.assertTrue(os.path.isdir(analyzing_dir))

        results, errors = Analyzer._Analyzer__analyze_from_files_paths(analyzing_dir, files_paths)

        self.assertNotEqual(0, len(errors))
        self.assertFalse(os.path.isdir(analyzing_dir))

    def analyze_success(self, files_paths):
        analyzing_dir = analyzer.setup_analyzing_dir()

        self.assertTrue(os.path.isdir(analyzing_dir))

        results, errors = Analyzer._Analyzer__analyze_from_files_paths(analyzing_dir, files_paths)
        analysis_result_keys = results.keys()

        self.assertItemsEqual(testconfig.EXPECTED_ANALYSIS_KEYS, analysis_result_keys)
        self.assertEqual(0, len(errors))
        self.assertFalse(os.path.isdir(analyzing_dir))


if __name__ == '__main__':
    unittest.main()
