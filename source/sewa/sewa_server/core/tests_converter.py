import os
import unittest

import testconfig
from core.converter import Converter


class ConverterTests(unittest.TestCase):
    def test_convert_webm(self):
        unconverted_files_paths = [testconfig.TEST_WEBM]
        self.check_conversion_success(unconverted_files_paths)

    def test_convert_invalid(self):
        unconverted_files_paths = ["nonexistantdir/nonexistantfile.nonexistantextension"]
        self.check_conversion_failure(unconverted_files_paths)

    def check_conversion_success(self, unconverted_files_paths):
        converted_files_paths = Converter.convert(unconverted_files_paths)

        # cleanup
        for converted_file_path in converted_files_paths:
            os.remove(converted_file_path)

        # Assert
        self.assertEqual(len(testconfig.EXPECTED_CONVERSION_EXTENSIONS), len(converted_files_paths))

        extensions = []
        for converted_file_path in converted_files_paths:
            name, extension = os.path.splitext(converted_file_path)
            extensions.append(extension)

        self.assertItemsEqual(testconfig.EXPECTED_CONVERSION_EXTENSIONS, extensions)

    def check_conversion_failure(self, unconverted_files_paths):
        converted_files_paths = Converter.convert(unconverted_files_paths)

        # Assert
        self.assertEqual(0, len(converted_files_paths))


if __name__ == '__main__':
    unittest.main()
