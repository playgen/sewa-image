import os

HOST = "localhost"
PORT = 8080

BASE_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

TEST_WEBM = BASE_DIR + "/test_data/test_short_audio_and_video.webm"
TEST_AVI = BASE_DIR + "/test_data/test_short_video_only.avi"
TEST_WAV = BASE_DIR + "/test_data/test_short_audio_only.wav"
EXPECTED_AUDIO_ANALYSIS_KEYS = ("Arousal", "Liking", "Valence")
EXPECTED_VIDEO_ANALYSIS_KEYS = ()
EXPECTED_ANALYSIS_KEYS = EXPECTED_AUDIO_ANALYSIS_KEYS + EXPECTED_VIDEO_ANALYSIS_KEYS
EXPECTED_CONVERSION_EXTENSIONS = [".avi", ".wav"]
