Here is the SEWA facial landmark tracker.

To install the tool, you will need:

1) Miniconda: https://conda.io/miniconda.html

2) Create the 'sewa' environment: conda create --name sewa python=2.7

3) Activate the 'sewa' environment: activate sewa (Windows) or source activate sewa (Ubuntu)

4) Dlib: conda install -c menpo dlib

5) OpenCV: conda install -c menpo opencv

6) ImageIO: conda install -c menpo imageio

To test the tool, please run test_sewa_tracker.bat (Windows) or test_sewa_tracker.sh (Ubuntu).
