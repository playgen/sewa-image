import os
import cv2
import sys
import dlib
import getopt
import imageio


def detect_face_dlib(dlib_detector, gray_frame):
    faces = dlib_detector(gray_frame)
    if len(faces) > 0:
        max_face = faces[0]
        for i in range(1, len(faces)):
            if max_face.area() < faces[i].area():
                max_face = faces[i]
        return True, max_face
    else:
        return False, dlib.rectangle(0, 0, 0, 0)


def get_bounding_box(pts):
    left = top = pts.part(17).x
    right = bottom = pts.part(17).y
    for i in range(18, pts.num_parts):
        left = min(left, pts.part(i).x)
        top = min(top, pts.part(i).y)
        right = max(right, pts.part(i).x)
        bottom = max(bottom, pts.part(i).y)
    return left, top, right, bottom


def calculate_face_box_margin(pts, face_box):
    left, top, right, bottom = get_bounding_box(pts)
    width = max(right - left, 1e-9)
    height = max(bottom - top, 1e-9)
    return float(face_box.left() - left) / width, float(face_box.top() - top) / height, \
           float(face_box.right() - right) / height, float(face_box.bottom() - bottom) / height


def apply_face_box_margin(pts, margin):
    left, top, right, bottom = get_bounding_box(pts)
    width = max(right - left, 1e-9)
    height = max(bottom - top, 1e-9)
    return dlib.rectangle(long(left + margin[0] * width), long(top + margin[1] * height),
                          long(right + margin[2] * width), long(bottom + margin[3] * height))


def save_pts(pts, pts_path):
    pts_file = open(pts_path, 'w')
    pts_file.write('version: 1\n')
    if pts is None or pts.num_parts < 19:
        pts_file.write('n_points: 0\n{\n}\n')
    else:
        pts_file.write('n_points: {0}\n{{\n'.format(pts.num_parts - 19))
        for i in range(17, pts.num_parts):
            if i != 60 and i != 64:
                pts_file.write('{0} {1}\n'.format(pts.part(i).x, pts.part(i).y))
        pts_file.write('}\n')
    pts_file.close()


def track_clip(input_video_file, output_folder, output_video_file=None, face_detection_interval=0.333333):

    # Load the models
    dlib_detector = dlib.get_frontal_face_detector()
    dlib_tracker = dlib.shape_predictor(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                                     'models', 'shape_predictor_68_face_landmarks.dat'))

    # Open the video file
    input_video = imageio.get_reader(input_video_file)
    fps = input_video.get_meta_data()['fps']
    face_detection_interval = max(round(face_detection_interval * fps), 1)

    # If necessary, create the directory for output pts files
    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)

    # If necessary, create the output video file
    if output_video_file is not None:
        output_video = imageio.get_writer(output_video_file, fps=fps)

    # The tracking process
    frame_number = 0
    update_margin = True
    for frame_number in range(0, input_video.get_length()):
        frame = input_video.get_data(frame_number)
        gray_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        if frame_number % face_detection_interval == 0:
            face_detected, face_box = detect_face_dlib(dlib_detector, gray_frame)
            update_margin = True
        if face_detected:
            if update_margin:
                pts = dlib_tracker(gray_frame, face_box)
                margin = calculate_face_box_margin(pts, face_box)
                update_margin = False
            else:
                face_box = apply_face_box_margin(pts, margin)
                pts = dlib_tracker(gray_frame, face_box)
            if output_video_file is not None:
                cv2.rectangle(frame, (face_box.left(), face_box.top()),
                              (face_box.right(), face_box.bottom()), (0, 255, 0), 2)
                for i in range(17, pts.num_parts):
                    if i != 60 and i != 64:
                        cv2.circle(frame, (pts.part(i).x, pts.part(i).y), 1, (255, 0, 0), 2)
        else:
            pts = None
        save_pts(pts, os.path.join(output_folder, '{0:06d}.pts'.format(frame_number + 1)))
        if output_video_file is not None:
            output_video.append_data(frame)
    if output_video_file is not None:
        output_video.close()


def main(argv):
    input_video_file = None
    output_folder = None
    output_video_file = None
    face_detection_interval = 0.333333
    opts, args = getopt.getopt(argv, 'i:o:v:f:', ['ivid=', 'ofolder=', 'ovid=', 'face='])
    for opt, arg in opts:
        if opt in ("-i", "--ivid"):
            input_video_file = arg
        elif opt in ("-o", "--ofolder"):
            output_folder = arg
        elif opt in ("-v", "--ovid"):
            output_video_file = arg
        elif opt in ("-f", "--face"):
            face_detection_interval = float(arg)
    if input_video_file is None:
        print 'Please specify the input video file path!'
    elif output_folder is None:
        print 'Please specify the output folder path!'
    else:
        print 'Input video file path: ' + input_video_file
        print 'Output folder path: ' + output_folder
        if output_video_file is not None:
            print 'Output video file path: ' + output_video_file
        print 'Face detection interval: {0} second'.format(face_detection_interval)
        track_clip(input_video_file, output_folder, output_video_file, face_detection_interval)
        print 'Facial landmark tracking completed.'


if __name__ == "__main__":
    main(sys.argv[1:])
