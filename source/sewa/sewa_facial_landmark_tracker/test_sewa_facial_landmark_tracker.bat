call activate sewa
if not exist "../test_data/Round2_SessionId_46_UserId_7d626c04-e5a1-45a5-ba95-2af2ec27b00b" (
    mkdir "../test_data/Round2_SessionId_46_UserId_7d626c04-e5a1-45a5-ba95-2af2ec27b00b"
)
python sewa_facial_landmark_tracker.py ^
    -i "../test_data/Round2_SessionId_46_UserId_7d626c04-e5a1-45a5-ba95-2af2ec27b00b.avi" ^
    -o "../test_data/Round2_SessionId_46_UserId_7d626c04-e5a1-45a5-ba95-2af2ec27b00b/Facial_Landmarks" ^
    -v "../test_data/Round2_SessionId_46_UserId_7d626c04-e5a1-45a5-ba95-2af2ec27b00b/Facial_Landmarks.mp4" ^
    -f 0.333333
if not exist "../test_data/Round2_SessionId_46_UserId_f680483e-b642-4542-86b2-02e0145a2191" (
    mkdir "../test_data/Round2_SessionId_46_UserId_f680483e-b642-4542-86b2-02e0145a2191"
)
python sewa_facial_landmark_tracker.py ^
    -i "../test_data/Round2_SessionId_46_UserId_f680483e-b642-4542-86b2-02e0145a2191.avi" ^
    -o "../test_data/Round2_SessionId_46_UserId_f680483e-b642-4542-86b2-02e0145a2191/Facial_Landmarks" ^
    -v "../test_data/Round2_SessionId_46_UserId_f680483e-b642-4542-86b2-02e0145a2191/Facial_Landmarks.mp4" ^
    -f 0.333333
call deactivate sewa
