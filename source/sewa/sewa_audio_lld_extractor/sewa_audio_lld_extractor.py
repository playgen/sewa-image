import os
import sys
import getopt


def main(argv):
    input_wav = None
    output_file = None
    opts, args = getopt.getopt(argv, 'i:o:', ['iwav=', 'ofile='])
    for opt, arg in opts:
        if opt in ("-i", "--iwav"):
            input_wav = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg

    process(input_wav, output_file)


def process(input_wav, output_file):
    if input_wav is None:
        print 'Please specify the input WAV file path!'
    elif output_file is None:
        print 'Please specify the output CSV file path!'
    else:
        print 'Input WAV file path: ' + input_wav
        print 'Output CSV file path: ' + output_file
        input_wav = os.path.realpath(input_wav)
        output_file = os.path.realpath(output_file)
        if os.name == 'nt':
            command = 'SMILExtract_Release.exe -C "ComParE_LLD.conf" -nologfile -I "' + input_wav + \
                      '" -instname "' + input_wav + '" -csvoutput "' + output_file + '" -l 1'
        else:
            command = './SMILExtract -C "ComParE_LLD.conf" -nologfile -I "' + input_wav + \
                      '" -instname "' + input_wav + '" -csvoutput "' + output_file + '" -l 1'
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        os.system(command)
        print 'Audio LLD extracted.'


if __name__ == "__main__":
    main(sys.argv[1:])
