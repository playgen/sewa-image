#!/usr/bin/env bash
source activate sewa
mkdir -p "../test_data/Round2_SessionId_46"
python sewa_audio_lld_extractor.py -i "../test_data/Round2_SessionId_46.wav" \
    -o "../test_data/Round2_SessionId_46/LLD.csv"
source deactivate sewa
