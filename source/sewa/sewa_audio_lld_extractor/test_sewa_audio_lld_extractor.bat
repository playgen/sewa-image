call activate sewa
if not exist "../test_data/Round2_SessionId_46" (
    mkdir "../test_data/Round2_SessionId_46"
)
python sewa_audio_lld_extractor.py -i "../test_data/Round2_SessionId_46.wav" ^
    -o "../test_data/Round2_SessionId_46/LLD.csv"
call deactivate sewa
